\chapter{Simulate multiple drones}

Cool. We managed to simulate one drone in Gazebo so far. Now if you try to add a second drone in the simulation, you will quickly realize that your drones cannot fly. They crash immediately. You get a behavior like the one presented in this video: \href{https://www.youtube.com/watch?v=mJ2B3DeYhfQ}{https://www.youtube.com/watch?v=mJ2B3DeYhfQ}

The reason for that is because as we explained earlier, each drone model in gazebo sends data to PX4 and listens for commands from PX4 to the default MAVLink simulation UDP port \code{14560}. When you add more than one drones in the simulated environment, all of them receive and post data in the same UDP port. Eventually PX4 (the autopilot software) sees a stream of mixed data from different drones and perceives it as one crazy drone. PX4 sends flight commands to fly this crazy drone, which is actually many drones (since all the drones listen for commands to the same port), thus, the drones crash after a little while.

To overcome this problem, we need to create additional gazebo drone models that listen to different UDP ports, and start one PX4 instance and one MAVROS instance per simulated drone. It is a cumbersome and task, so keep on reading to understand how to achieve that. In the end of this chapter we will be having two drones in one simulation. One will be the default Iris drone model, and the second one will be the default Iris drone with a 360 RPLidar sensor and flow cam.

\section{Create additional drone models}
First we need to copy the Gazebo models of the drones we have to simulate:

\begin{lstlisting}[style=bash]
# Copy the iris_rplidar model to a new iris_rplidar_2 directory
rsync -avP ~/src/Firmware/Tools/sitl_gazebo/models/iris_rplidar/ ~/src/Firmware/Tools/sitl_gazebo/models/iris_rplidar_2/
cd ~/src/Firmware/Tools/sitl_gazebo/models/iris_rplidar_2/
mv iris_rplidar.sdf iris_rplidar_2.sdf
# Change the name of the model to avoid having name clashes in your simulation
sed -i "s|<model name='iris_rplidar'>|<model name='iris_rplidar_2'>|" iris_rplidar_2.sdf
# Tell the model.config which file describes our new model.
sed -i 's|<sdf>iris_rplidar.sdf<|<sdf>iris_rplidar_2.sdf<|' model.config
# Give the new model a slightly different name to differentiate it from the original.
sed -i 's|<name>3DR Iris with rplidar lidar<|<name>3DR Iris with rplidar lidar (2)<|' model.config
\end{lstlisting}

If you now open the file \code{iris\_rplidar\_2.sdf}, you will notice that there is no UDP port defined in there. Where is the UDP port that the drone model will use to communicate with PX4? The \code{iris\_rplidar} model is basically an \code{iris} model with some sensors attached (each sensor is a different model itself). Thus, you will see there is an \code{include} XML element in the file \code{iris\_rplidar\_2.sdf} that contains the line \code{<uri>model://iris</uri>}. So the \code{iris\_rplidar} model depends on the \code{iris} model, and this is where the PX4 communication port is defined:
\begin{lstlisting}[style=bash]
$ grep -n 14560 ~/src/Firmware/Tools/sitl_gazebo/models/iris/iris.sdf 
429:      <mavlink_udp_port>14560</mavlink_udp_port>
\end{lstlisting}

We will then need to create an \code{iris\_2} copy of the original \code{iris} model and make the \code{iris\_rplidar\_2.sdf} depend on the \code{iris\_2} model. Unfortunately the SDF file specification does not allow to override parameters in included models\footnote{\href{http://answers.gazebosim.org/question/2764/passing-parameters-to-included-sdf/}{http://answers.gazebosim.org/question/2764/passing-parameters-to-included-sdf/}}\footnote{\href{https://bitbucket.org/osrf/gazebo/issues/1009/give-parameters-into-included-models-in-a}{https://bitbucket.org/osrf/gazebo/issues/1009/give-parameters-into-included-models-in-a}}. That is why we have to do it in this cumbersome way.

\begin{lstlisting}[style=bash]
# In the iris_rplidar_2 model you must load the iris_2 model that we're going to create next.
sed -i 's|<uri>model://iris<|<uri>model://iris_2<|' ~/src/Firmware/Tools/sitl_gazebo/models/iris_rplidar_2/iris_rplidar_2.sdf 

# Create the iris_2 model in a similar way as we did with the iris_rplidar_2 model earlier
rsync -avP ~/src/Firmware/Tools/sitl_gazebo/models/iris/ ~/src/Firmware/Tools/sitl_gazebo/models/iris_2/
cd ~/src/Firmware/Tools/sitl_gazebo/models/iris_2/
mv iris.sdf iris_2.sdf
# Change the communication port of the iris_2 model to 14590
sed -i 's|<mavlink_udp_port>14560<|<mavlink_udp_port>14590<|' iris_2.sdf
sed -i 's|<name>3DR Iris<|<name>3DR Iris (2)<|' model.config
sed -i "s|<sdf version='1.4'>iris.sdf<|<sdf version='1.4'>iris_2.sdf<|" model.config
\end{lstlisting}

At this point you have the default 3DS Iris-based models (\code{iris} and \code{iris\_rplidar}) that can be controlled in the default UDP port \code{14590}, and you created two more 3DS Iris-based models (\code{iris\_2} and \code{iris\_rplidar\_2}) that can be controlled in the UDP port \code{14590}.

If you want to be able to simulate more than two drones, then you should create even more models by following the same steps and use different UDP ports for communication (e.g. create an \code{iris\_3} model listening to port \code{14620}, \code{iris\_4} listening to port \code{14650} etc).

\red{NOTE}: If you check the file \code{iris\_rplidar\_2.sdf}, you will see that more sdf models (not only the \code{iris}) are included to build the \code{iris\_rplidar} model. For example, a camera (\code{flow\_cam}), a \code{lidar} and an \code{rplidar} (360 degrees lidar) sensors are included. The \code{rplidar} is publishing data in the ROS topic \code{laser/scan} as defined in the file \code{\mytilde/src/Firmware/Tools/sitl\_gazebo/models/rplidar/model.sdf}. That means that if you want to use two \code{iris\_rplidar} drones in the same simulation and read data from both rplidar sensors, you should make a second copy for each of these sensors and modify the sdf files accordingly as we already did for the \code{iris} model above (it's a nightmare). The camera and lidar are also publishing data in some hardcoded Gazebo topics (Gazebo topics are slightly different than ROS topics and you can still access them, but in a different way. Use the \code{gz topic -l} command to list the gazebo topics. I won't go into details on that), so the list keeps on going.

\section{Spawn two (or more) px4 instances that will be used to control the drones}

A convenient script is already provided (\code{\mytilde/src/Firmware/Tools/sitl\_multiple\_run.sh}) by PX4 for that purpose, but we need to do some minor modifications:

We will apply the following patch to the file \mytilde/src/Firmware/Tools/sitl\_multiple\_run.sh:

\begin{lstlisting}[style=diff]
diff --git a/Tools/sitl_multiple_run.sh b/Tools/sitl_multiple_run.sh
index 7f590d8..cdd531f 100755
--- a/Tools/sitl_multiple_run.sh
+++ b/Tools/sitl_multiple_run.sh
@@ -4,14 +4,14 @@

sitl_num=2

-sim_port=15019
-mav_port=15010
-mav_port2=15011
+sim_port=14560
+mav_port=14556
+mav_port2=14557

-mav_oport=15015
-mav_oport2=15016
+mav_oport=14550
+mav_oport2=14540

-port_step=10
+port_step=30

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
src_path="$SCRIPT_DIR/.."
@@ -30,6 +30,7 @@ user=`whoami`
n=1
while [ $n -le $sitl_num ]; do
working_dir="instance_$n"
+       rm -rf "$working_dir" &> /dev/null
if [ ! -d $working_dir ]; then
mkdir -p "$working_dir"
pushd "$working_dir" &>/dev/null
@@ -43,7 +44,7 @@ while [ $n -le $sitl_num ]; do

pushd "$working_dir" &>/dev/null
echo "starting instance $n in $(pwd)"
-       sudo -b -u $user ../src/firmware/posix/px4 -d "$src_path" rcS >out.log 2>err.log
+       sudo -b -u $user ../px4 -d "$src_path" rcS >out.log 2>err.log
popd &>/dev/null

n=$(($n + 1))
\end{lstlisting}

Run these commands to apply the patch:
\begin{lstlisting}[style=bash]
# gzipped, base64 encoded patch
patch_base64="H4sIAIZBFFoAA42SW2+bMBTHn+NPcYZQlYiaSwLJqokoWzepfdqUdE9VRSk2wSrYyEATadp37zFd
SnZRuhdzrj+f8zdM5DlQuhUtpN6NUmXjNaItk6orW1GXPNGddJsCHk4kiZCM72GRRxc+e++6GWPR
LMgh8P1FFBFK6Uk2cRznNH+1AhqeByE4/blaESDQV8quiqfo0UZUSa10GweRH1wQWqVPg+8P/rQP
BMQZGsJo7hNnaAijaD740z6wMHeYkHqFRkeBF+qvLvWK8Y8CPSf0Dce4SdPyOsbJnMGbmSxsLtfX
326Sz9fr2LLHkDEwHya0TCuO9o9PHzdXyebr9/Xll1v/7qcFEwvOzqDeMbRQF50lddoW2D2gPNe1
ehln/vkcHDwXKCN0Ddfx/a5QaSXuCcg4ILArRMnhFmwJFA37oDPcfQCmCIx2Sj8KuU1wpNgSsmlT
mfHElhZxRroCqnOc8qgIp1uCx/iTJ7uyRIDIEf8OKIPjMsNvCy6xYFQ9mgCt/wCZVN01Bfv7goHf
/ywz3M8Jw5ct39oIsW9SYcSzQoGF2+oWK+CwuKEKCfYY9Uf56ajpmAL6ALQD2+gLruvhm3i50NUu
1dyrVSP2Xr0PjQKWfXgvC3S2gaXqWrdUW5guudbGQlX/hfz/ftxO1ez3bTAoY3s8xuEdCCYT8gx2
r56wCAQAAA=="
# Go into the folder that contains the file sitl_multiple_run.sh
cd ~/src/Firmware/Tools
# Apply the patch
echo "${patch_base64}" | base64 -d | gunzip | patch
\end{lstlisting}

Now run the script:
\begin{lstlisting}[style=bash]
~/src/Firmware/Tools/sitl_multiple_run.sh
\end{lstlisting}

This script will create two directories (\code{\mytilde/src/Firmware/build/posix\_sitl\_default/instance\_1} and \code{\mytilde/src/Firmware/build/posix\_sitl\_default/instance\_2}) with the necessary config files to run two independent PX4 instances for controlling two different drones. \code{instance\_1} will be controlling the drone that listens to the port 14560, and \code{instance\_2} will be controlling the drone that listens to the port 14590 that we created earlier.

\section{Create a world with two drones}
At this point we have managed to configure two Gazebo drones that will communicate with two independent PX4 instances. We still need to put these two drones in the same Gazebo world. So let's prepare a Gazebo world that is based on the existing \code{iris.world}:

\begin{lstlisting}[style=bash]
# Copy the iris.world to and iris_2.world
rsync -avP ~/src/Firmware/Tools/sitl_gazebo/worlds/iris.world ~/src/Firmware/Tools/sitl_gazebo/worlds/iris_2.world
\end{lstlisting}

Then you need to edit the \code{iris\_2.world} file and right after these lines:
\begin{lstlisting}[style=xml]
<include>
  <uri>model://iris</uri>
</include>
\end{lstlisting}

you should append these lines:
\begin{lstlisting}[style=xml]
<include>
  <uri>model://iris_rplidar_2</uri>
  <pose>7 0 0 0 0 0</pose>
</include>
\end{lstlisting}

You can append these lines with a single command as follows (you will first need to install \code{xmlstarlet} if it's not already present):
\begin{lstlisting}[style=bash]
sudo apt-get install xmlstarlet
# This command will insert (prepend) an empty include element, right before the physics
# element. This will become the last "include" element in this XML file. Then in the last()
# include element the "uri" and "pose" elements will be added with the correct values. 
xmlstarlet ed --inplace \
    -i '/sdf/world[@name="default"]/physics[@name="default_physics"]' \
        -t elem -name "include" -v "" \
    -s '/sdf/world[@name="default"]/include[last()]' \
        -t elem -name "uri" -v "model://iris_rplidar_2" \
    -s '/sdf/world[@name="default"]/include[last()]' \
        -t elem -name "pose" -v "7 0 0 0 0 0" \
    ~/src/Firmware/Tools/sitl_gazebo/worlds/iris_2.world
\end{lstlisting}

The resulting world will be hosting one \code{iris} model, and our newly created \code{iris\_rplidar\_2} model. Note that in order to not put one drone model on top of each other, we move the \code{iris\_rplidar\_2} model 7 units\footnote{This \textit{unit} mentioned in this sentence is an arbitrary unit that represents coordinates in the Gazebo world. Think of it as 7 meters in this case.} in the X axis

\section{Modify MAVROS roslaunch files to allow controlling different drones}
The default \code{roslaunch mavros px4.launch} command that we executed in section~\ref{sec:run-first-simulation} will always give the name \code{mavros} to the ROS node that we need to use to control the drones, thus, we can only control one drone at a time. To work around that issue\footnote{I searched quite a lot to find a solution that would allow us just to pass the ROS node name as a variable in roslaunch, but I couldn't find any. You can change a ROS node name if you start the node manually, but then you would need to run multiple additional commands manually. Think of roslaunch as a utility that coordinates how a ROS service that is composed of multiple nodes should start.}, we will need to create a new \code{px4\_2.launch} file with the following commands:

\begin{lstlisting}[style=bash]
# Copy the node.launch to a new file named node_.launch. In that new file change the
# mavros node name from 'mavros' to 'mavros_2'
rsync -avP ~/catkin_ws/src/mavros/mavros/launch/node.launch ~/catkin_ws/src/mavros/mavros/launch/node_2.launch
sed -i 's|name="mavros"|name="mavros_2"|' ~/catkin_ws/src/mavros/mavros/launch/node_2.launch

# Now copy the roslaunch file 'px4.launch' to a new roslaunch file 'px4_2.launch'
# Configure the new file to use node_2.launch.
rsync -avP ~/catkin_ws/src/mavros/mavros/launch/px4.launch ~/catkin_ws/src/mavros/mavros/launch/px4_2.launch
sed -i 's|launch/node.launch">|launch/node_2.launch">|' ~/catkin_ws/src/mavros/mavros/launch/px4_2.launch
\end{lstlisting}

%Now if use `roslaunch` two start the two mavros instances. The default `px4.launch` will launch a mavros node named `mavros`, and the newly created `px4_2.launch` will create mavros node named `mavros_2`

%```
%source ~/catkin_ws/devel/setup.bash && roslaunch mavros px4.launch fcu_url:="udp://:14552@127.0.0.1:14557"
%source ~/catkin_ws/devel/setup.bash && roslaunch mavros px4_2.launch fcu_url:="udp://:14562@127.0.0.1:14567"
%```


\section{Run the simulation with two drones}
At this point, the remaining steps are similar to the ones described in section~\ref{sec:run-first-simulation}. We only have to ensure that we use our newly created world in gazebo, and we need two run two mavros instances with different ROS node names in order to control the two drones.

\begin{lstlisting}[style=bash]
# In shell 1 start roscore:
roscore

# In shell 2 start px4 instances:
# We already executed this script earlier, but the script takes care to kill the
# existing px4 instances, so it doesn't hurt to rerun it.
cd ~/src/Firmware/build/posix_sitl_default
~/src/Firmware/Tools/sitl_multiple_run.sh

# In shell 3 start gazebo with the iris_2.world:
export DISPLAY=:1
/opt/TurboVNC/bin/vncserver -list |
awk '{print $1}' | while read vncdisplay; do
if [[ ${vncdisplay:0:1} == ":" ]]; then
/opt/TurboVNC/bin/vncserver -kill ${vncdisplay}
fi
done
/opt/TurboVNC/bin/vncserver
export PX4_HOME_LAT=59.916670
export PX4_HOME_LON=10.729153
export PX4_HOME_ALT=2.5
cd ~/src/Firmware
source Tools/setup_gazebo.bash $(pwd) $(pwd)/build/posix_sitl_default
vglrun roslaunch gazebo_ros empty_world.launch world_name:=$(pwd)/Tools/sitl_gazebo/worlds/iris_2.world

# In shell 4 start the first mavros instance to control the first drone:
source ~/catkin_ws/devel/setup.bash && roslaunch mavros px4.launch fcu_url:="udp://:14540@127.0.0.1:14557"

# In shell 5 start the second mavros instance to control the second drone:
source ~/catkin_ws/devel/setup.bash && roslaunch mavros px4_2.launch fcu_url:="udp://:14570@127.0.0.1:14587"

# In shell 6 you can optionally start qground control:
export DISPLAY=:1
vglrun ~/qgroundcontrol/qgroundcontrol-start.sh

# In shell 7 run some basic commands to lift the drones in the air:
# First the first Drone
# Arm the first drone
rosservice call /mavros/cmd/arming "value: true"

# Take off the first drone
rosservice call /mavros/cmd/takeoff "{min_pitch: 0.0, yaw: 0.0, latitude: 59.916670, longitude: 10.729153, altitude: 2.0}"

# Land the first drone
rosservice call /mavros/cmd/land "{min_pitch: 0.0, yaw: 0.0, latitude: 59.916670, longitude: 10.729153, altitude: 2.0}"

# Then the second Drone
# Arm the second drone
rosservice call /mavros_2/cmd/arming "value: true"

# Take off the second drone
rosservice call /mavros_2/cmd/takeoff "{min_pitch: 0.0, yaw: 0.0, latitude: 59.916670, longitude: 10.729200, altitude: 2.0}"

# Land the second drone
rosservice call /mavros_2/cmd/land "{min_pitch: 0.0, yaw: 0.0, latitude: 59.916670, longitude: 10.729200, altitude: 2.0}"
\end{lstlisting}


\section{Configure QGroundControl to connect to different simulated drones}
QGroundControl by default listens to port \code{14550} for incoming data. This is the default port defined by the MAVLink protocol for communication with ground control stations. As such, by default when launching QGroundControl in a simulation with more than one drones, QGroundControl will always connect to the first drone in your simulation (if you have any PX4 instance configured to send data to the default port \code{14550}). If you want to connect to a different drone\footnote{Note that QGroundControl can only be connected to one drone at a time, unless you can start multiple instances, something that I haven't tried.}, follow the instructions in the captions of the Figures~\ref{fig:config-qgc-addlink-1},~\ref{fig:config-qgc-addlink-2},~\ref{fig:config-qgc-addlink-3}:

\begin{figure}[t!]
	\centering
	\includegraphics[width=17cm]{figures/QGroundControl-AddCommLink-1.png}
	\caption{First go to the \textit{Comm Links} page and press the \textbf{Add} button.}
	\label{fig:config-qgc-addlink-1}
\end{figure}

\begin{figure}[t!]
	\centering
	\includegraphics[width=17cm]{figures/QGroundControl-AddCommLink-2.png}
	\caption{Choose the \textbf{UDP} connection \textit{Type}. Change the name to something meaningful to help you identify the drone you are connecting to. Choose the \textit{Listening Port} \textbf{14580} to communicate with the second drone. Click the \textbf{OK} button. The script \textit{sitl\_multiple\_run.sh} that we modified earlier is increasing the port numbers by 30 for each additional drone. So since we start from the default 14550 port, we can connect to the second drone on port 14580, the third one on port 14610 etc.}
	\label{fig:config-qgc-addlink-2}
\end{figure}

\begin{figure}[t!]
	\centering
	\includegraphics[width=17cm]{figures/QGroundControl-AddCommLink-3.png}
	\caption{Click on the newly appeared entry and click the \textbf{Connect} button to connect to the second drone.}
	\label{fig:config-qgc-addlink-3}
\end{figure}

\red{NOTE:} After following the instructions on how to connect to the second drone in the simulation, you must have noticed that QGroundControl is \textit{confused} and tries to talk to both of the simulated drones. This is happening because QGC always listens to port \code{14550} no matter what. That means that even if you connect to a second drone on a port other than \code{14550} (as we did), if you have any active drone that sends data to the port \code{14550}, QGC will be confused because it will be reading data from more than one drones simultaneously. In this case, make sure that you have no drones operating in the default ports when you simulate multiple drones and you have the need to use QGC to connect to more than one drones. In order to avoid using the default ports, you need to modify the starting ports that are defined in the script \code{sitl\_multiple\_run.sh}.

\section{Automating the multidrone simulation}
As you understood after this long write in this chapter, in order to simulate many drones it is a pain and an error prone task if you have to edit all of these different files manually. In order to ease our pain, I created a script that automates this task.

Download the script and place it in the \code{Tools} folder of the PX4 source. Make it executable and run it:

\begin{lstlisting}[style=bash]
cd ~/src/Firmware/Tools
wget --user=vangelis@tasoulas.net --ask-password https://bitbucket.org/hioarobotics/multidrone-simulation/raw/2ba58ee656b7a2877ab57009092f1f4880eeb706/scripts/sitl_run_gazebo_multidrone.sh
chmod +x sitl_run_gazebo_multidrone.sh
./sitl_run_gazebo_multidrone.sh
\end{lstlisting}

By default, the script will prepare two \code{iris\_rplidar} drones that can be controlled with MAVROS in the nodes \code{mavros\_1} and \code{mavros\_2} respectively.

You can run the script with an positive integer argument greater than two, in order to simulate even more drones. A sample execution and the output of the script when we ask to simulate 3 drones:
\begin{lstlisting}[style=bash]
$ ~/src/Firmware/Tools/sitl_run_gazebo_multidrone.sh 3
killing all px4 instances
killing all mavros_node instances
---------

Creating a copy of the iris_rplidar drone model (1)
iris_rplidar_1 depends on the iris model.
Creating a copy of the iris drone model (1)
iris_rplidar_1 depends on the lidar model.
Creating a copy of the lidar model (1)
iris_rplidar_1 depends on the rplidar model.
Creating a copy of the rplidar model (1)
iris_rplidar_1 depends on the flow_cam model.
Creating a copy of the flow_cam model (1)
Creating mavros launch files

Starting instance 1 in /home/vangelis/px4_instances/px4_instance_1
PX4 will be broadcasting data to the:
* Offboard API (MAVROS) from the src port '14557' to the dst port '14540'
* QGroundControl from the src port '14556' to the dst port '14550'
* Simulator from a random src port to the dst port '14560'

The following 'roslaunch mavros' command will now be executed:
roslaunch mavros px4_1.launch fcu_url:="udp://:14540@127.0.0.1:14557"

You can find the mavros_1 stdout and stderr in the files /home/vangelis/px4_instances/px4_instance_1/mavros.log and /home/vangelis/px4_instances/px4_instance_1/mavros.err.log
For the full logging please check '/home/vangelis/.ros/log/6140e0e6-cede-11e7-9fb4-4ccc6a283edb/mavros_1-1.log'
---------

Creating a copy of the iris_rplidar drone model (2)
iris_rplidar_2 depends on the iris model.
Creating a copy of the iris drone model (2)
iris_rplidar_2 depends on the lidar model.
Creating a copy of the lidar model (2)
iris_rplidar_2 depends on the rplidar model.
Creating a copy of the rplidar model (2)
iris_rplidar_2 depends on the flow_cam model.
Creating a copy of the flow_cam model (2)
Creating mavros launch files

Starting instance 2 in /home/vangelis/px4_instances/px4_instance_2
PX4 will be broadcasting data to the:
* Offboard API (MAVROS) from the src port '14587' to the dst port '14570'
* QGroundControl from the src port '14586' to the dst port '14580'
* Simulator from a random src port to the dst port '14590'

The following 'roslaunch mavros' command will now be executed:
roslaunch mavros px4_2.launch fcu_url:="udp://:14570@127.0.0.1:14587"

You can find the mavros_2 stdout and stderr in the files /home/vangelis/px4_instances/px4_instance_2/mavros.log and /home/vangelis/px4_instances/px4_instance_2/mavros.err.log
For the full logging please check '/home/vangelis/.ros/log/6140e0e6-cede-11e7-9fb4-4ccc6a283edb/mavros_2-1.log'
---------

Creating a copy of the iris_rplidar drone model (3)
iris_rplidar_3 depends on the iris model.
Creating a copy of the iris drone model (3)
iris_rplidar_3 depends on the lidar model.
Creating a copy of the lidar model (3)
iris_rplidar_3 depends on the rplidar model.
Creating a copy of the rplidar model (3)
iris_rplidar_3 depends on the flow_cam model.
Creating a copy of the flow_cam model (3)
Creating mavros launch files

Starting instance 3 in /home/vangelis/px4_instances/px4_instance_3
PX4 will be broadcasting data to the:
* Offboard API (MAVROS) from the src port '14617' to the dst port '14600'
* QGroundControl from the src port '14616' to the dst port '14610'
* Simulator from a random src port to the dst port '14620'

The following 'roslaunch mavros' command will now be executed:
roslaunch mavros px4_3.launch fcu_url:="udp://:14600@127.0.0.1:14617"

You can find the mavros_3 stdout and stderr in the files /home/vangelis/px4_instances/px4_instance_3/mavros.log and /home/vangelis/px4_instances/px4_instance_3/mavros.err.log
For the full logging please check '/home/vangelis/.ros/log/6140e0e6-cede-11e7-9fb4-4ccc6a283edb/mavros_3-1.log'

Creating a Gazebo world with 3 models
You can now execute the simulator with the following set of commands:

export PX4_HOME_LAT=59.916670
export PX4_HOME_LON=10.729153
export PX4_HOME_ALT=2.5
source /home/vangelis/src/Firmware/Tools/setup_gazebo.bash /home/vangelis/src/Firmware /home/vangelis/src/Firmware/build/posix_sitl_default
vglrun roslaunch gazebo_ros empty_world.launch world_name:=/home/vangelis/src/Firmware/Tools/sitl_gazebo/worlds/3_drones.world
\end{lstlisting}

To kill the PX4 and MAVROS instances that have been started by the script, you can issue the script with the \code{k} argument:
\begin{lstlisting}[style=bash]
$ ~/src/Firmware/Tools/sitl_run_gazebo_multidrone.sh k
killing all px4 instances
killing all mavros_node instances
Removing directory /home/vangelis/px4_instances/px4_instance_1
Removing directory /home/vangelis/px4_instances/px4_instance_3
Removing directory /home/vangelis/px4_instances/px4_instance_2
\end{lstlisting}
