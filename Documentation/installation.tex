\chapter{Installation}

In this section we will go through the installation of the necessary components and we will execute a very basic simulation by sending a few MAVROS commands from the command line.

\red{NOTE 1}: Since the simulator executes a program that requires 3D acceleration, you may need a powerful server with a good graphics card.

\red{NOTE 2}: If you want to execute your simulations remotely, you will want a way to forward 3D graphics over VNC. If that's the case, follow the instructions on how to setup VirtualGL and TurboVNC in this link before proceeding: \href{https://gist.github.com/cyberang3l/422a77a47bdc15a0824d5cca47e64ba2}{https://gist.github.com/cyberang3l/422a77a47bdc15a0824d5cca47e64ba2}.

\red{NOTE 3}: In the following set of commands, some commands are prepended with the command \code{vglrun}. This is the VirtualGL command that is required to execute a 3D accelerated software in the server and forward its output in a TurboVNC \code{\$DISPLAY} that is displaying content remotely. If you execute your simulations locally, the \code{vglrun} command is not needed (remove it from the corresponding command whenever you see it).

\red{NOTE 4}: Read the comments in the code boxes before copy/pasting the commands into a terminal.

\section{Installation of the necessary components to run simulations}
\label{sec:installation-of-components-for-simulation}

In the PX4 website here: \href{https://dev.px4.io/en/setup/dev_env_linux_ubuntu.html}{https://dev.px4.io/en/setup/dev\_env\_linux\_ubuntu.html} there is a convenient script for installation. The script will install git, ROS, Gazebo, clone the PX4 repository, and some extra libraries that are needed. Since the script downloads and compiles lots of stuff, it may take some time. Please be patient.

\begin{lstlisting}[style=bash]
wget https://raw.githubusercontent.com/PX4/Devguide/master/build_scripts/ubuntu_sim_ros_gazebo.sh
chmod +x ubuntu_sim_ros_gazebo.sh
./ubuntu_sim_ros_gazebo.sh
\end{lstlisting}

In the end, you should have the following new files and folders in your home directory:

\begin{lstlisting}[style=bash]
~/catkin_ws                     # Contains 'mavlink' and 'mavros' ROS packages
~/eProsima_FastCDR-1.0.7-Linux  # Some additional libraries that we do not need
~/eProsima_FastRTPS-1.5.0-Linux # (at least not in this early stage)
~/ninja                         # Contains the ninja build system binary
~/src                           # The folder where the PX4 firmware has been downloaded
\end{lstlisting}

The installation will also add the following two lines to your \code{\mytilde/.bashrc} file. These lines are needed to setup the ROS environment for you user. Again, if that doesn't make much sense to you, remember that you have to go through the ROS tutorials.

\begin{lstlisting}[style=bash]
source /opt/ros/kinetic/setup.bash
source ~/catkin_ws/devel/setup.bash
\end{lstlisting}

The ninja build system path will also be added in your \code{.profile \$PATH}:

\begin{lstlisting}[style=bash]
export PATH=~/ninja:$PATH
\end{lstlisting}

From all of these components, we mostly care about the \code{\mytilde/src/Firmware}. Go into that directory and compile the PX4 firmware now:
\begin{lstlisting}[style=bash]
cd ~/src/Firmware
cpucores=$(( $(lscpu | grep Core.*per.*socket | awk -F: '{print $2}') * $(lscpu | grep Socket\(s\) | awk -F: '{print $2}') ))
make -j$cpucores
make -j$cpucores posix_sitl_default sitl_gazebo
\end{lstlisting}

Download and install the GeographicLib datasets. These are required by the MAVROS package\footnote{\href{https://github.com/mavlink/mavros/tree/master/mavros\#required-dependencies}{https://github.com/mavlink/mavros/tree/master/mavros\#required-dependencies}}.
\begin{lstlisting}[style=bash]
cd ~/catkin_ws/src/mavros/mavros/scripts/
sudo ./install_geographiclib_datasets.sh
\end{lstlisting}

Now download qgroundcontrol:
\begin{lstlisting}[style=bash]
cd ~
wget https://s3-us-west-2.amazonaws.com/qgroundcontrol/latest/QGroundControl.tar.bz2
tar xvf QGroundControl.tar.bz2
rm QGroundControl.tar.bz2
sudo apt-get install libespeak1 libsdl2-2.0-0 -y # Packages needed by qgroundcontrol
sudo usermod -a -G dialout $USER
\end{lstlisting}

\section{Running our first simulation}
\label{sec:run-first-simulation}
Note that you will need several terminal windows or tabs for the following steps. Fire up your favorite terminal and let's begin.

\red{NOTE:} if you have some terminal windows already opened, logout and login again at this point in order to ensure that the correct ROS paths are in place. If you do not want to logout/login, you can source your \code{.profile} and \code{.bashrc} file like this:
\begin{lstlisting}[style=bash]
. ~/.profile
. ~/.bashrc
\end{lstlisting}

\subsubsection*{Shell 1: Run roscore}
\code{roscore} is needed whenever you want to execute any ROS related software.
\begin{lstlisting}[style=bash]
roscore
\end{lstlisting}

\subsubsection*{Shell 2: Run Gazebo simulator}
\begin{lstlisting}[style=bash]
# First a few optional steps that assume you need a VNC connection to the simulation
# server and you have already been through the VirtualGL installation instructions here:
#    https://gist.github.com/cyberang3l/422a77a47bdc15a0824d5cca47e64ba2

# Optional step 1 if you use VirtualGL and VNC. Kill all the existing vnc servers.
/opt/TurboVNC/bin/vncserver -list |
awk '{print $1}' | while read vncdisplay; do
    if [[ ${vncdisplay:0:1} == ":" ]]; then
        /opt/TurboVNC/bin/vncserver -kill ${vncdisplay}
    fi
done
# Optional step 2 if you use VirtualGL and VNC. Start a new VNC session.
/opt/TurboVNC/bin/vncserver
# Optional step 3 if you use VirtualGL and VNC. Setup where the graphical
# applications will be displayed.
export DISPLAY=:1 # Change :1 to the VNC DISPLAY that has been started by your turbovnc server
# Optional step 4 if you use VirtualGL and VNC. In your computer, open TurboVNC
# client and connect to the server.
/opt/TurboVNC/bin/vnclient # NOTE this is the only command that has to be run to your local computer. All the previous commands and the commands that follow, will have to be executed to the simulation server (which I assume is a remote server to you and you are connected to it with SSH).

# Setup a custom take off location before you start the gazebo simulator
# (https://dev.px4.io/en/simulation/gazebo.html).
# By default the simulation starts somewhere in Switzerland
# (the core PX4 team is located in Zurich).
# Take off location at slottsplassen.
export PX4_HOME_LAT=59.916670
export PX4_HOME_LON=10.729153
export PX4_HOME_ALT=30.0

# Load the necessary environment variables for the gazebo simulator.
cd ~/src/Firmware
source Tools/setup_gazebo.bash $(pwd) $(pwd)/build/posix_sitl_default

# Launch the simulator
vglrun roslaunch gazebo_ros empty_world.launch world_name:=$(pwd)/Tools/sitl_gazebo/worlds/iris.world
\end{lstlisting}

\subsubsection*{Shell 3: Run PX4}
Use a script that I made to allow you to easily start one PX4 instance that broadcasts data to the default ports. Note that the script assumes that the PX4 firmware location is in \code{\mytilde/src/Firmware}.
\begin{lstlisting}[style=bash]
cd ~
wget --user=vangelis@tasoulas.net --ask-password https://bitbucket.org/hioarobotics/multidrone-simulation/raw/c2608f1bda7c7d788f302d70e2146d63dae391d3/scripts/sitl_px4_run.sh
chmod +x sitl_px4_run.sh
./sitl_px4_run.sh
\end{lstlisting}

\subsubsection*{Shell 4: Run MAVROS}
\begin{lstlisting}[style=bash]
# Explanation about the fcu connection url in:
#   https://github.com/mavlink/mavros/blob/master/mavros/README.md
#
# udp://[bind_host][:port]@[remote_host[:port]]
#
# Glossary
#   GCS — Ground Control Station
#   FCU — Flight Control Unit (aka FC)
#   OBC — OnBoard Computer (your odroid or raspberry)
roslaunch mavros px4.launch fcu_url:="udp://:14540@127.0.0.1:14557"
\end{lstlisting}

\subsubsection*{Shell 5: Run QGroundControl}
This step is optional. If you want to see the route of your drone on a map and also have the ability to send commands with a GUI, you can now start qgroundcontrol\footnote{\href{https://docs.qgroundcontrol.com/en/getting_started/download_and_install.html\#ubuntu-linux}{https://docs.qgroundcontrol.com/en/getting\_started/download\_and\_install.html\#ubuntu-linux}}.
\begin{lstlisting}[style=bash]
# Assuming you have already extracted QGroundControl in the directory ~/qgroundcontrol
export DISPLAY=:1 # Optional if you used VirtualGL. Change :1 to the VNC DISPLAY
                  # that has been started by your turbovnc server
cd ~/qgroundcontrol
vglrun ./qgroundcontrol-start.sh
\end{lstlisting}

\subsubsection*{Shell 6: Fly your drone with some basic MAVROS commands.}
You can now fly the drone by using standard ROS commands (rosservice, rostopic etc). Look at the Gazebo window and you should see the 3D drone moving while you type different commands.
\begin{lstlisting}[style=bash]
# Arm the drone propellers
rosservice call /mavros/cmd/arming "value: true"

# Take off (probably not needed since we'll add a mission to the drone, but still
# doesn't hurt to take off and hover)
rosservice call /mavros/cmd/takeoff "{min_pitch: 0.0, yaw: 0.0, latitude: 59.916670, longitude: 10.729153, altitude: 2.0}"

# Push a mission with two waypoints
rosservice call /mavros/mission/push "start_index: 0 
waypoints: 
- {frame: 3, command: 22, is_current: true, autocontinue: true, param1: 0.0, param2: 0.0, 
	param3: 0.0, param4: 0.0, x_lat: 59.916770, y_long: 10.729353, z_alt: 5.0}
- {frame: 3, command: 16, is_current: false, autocontinue: true, param1: 0.0, param2: 0.0, 
	param3: 0.0, param4: 0.0, x_lat: 59.916670, y_long: 10.729153, z_alt: 1.0}"

# At this point you have to start the mission with the command 300:
# http://mavlink.org/messages/common#MAV_CMD_MISSION_START
rosservice call /mavros/cmd/command "{broadcast: false, command: 300, confirmation: 0, param1: 0.0, param2: 0.0, param3: 0.0,
	param4: 0.0, param5: 0.0, param6: 0.0, param7: 0.0}"

# Land the drone
rosservice call /mavros/cmd/land "{min_pitch: 0.0, yaw: 0.0, latitude: 59.916670, longitude: 10.729153, altitude: 0.0}"

# Clear the mission
rosservice call /mavros/mission/clear

# Now let's take off again and start publishing some "local position" 10 times per second.
# The local position will move the drone relative to its take off position. We do not
# need to use GPS coordinates in this case.
rostopic pub -r 10 /mavros/setpoint_position/local geometry_msgs/PoseStamped "{pose:{position:[10.0, 10.0, 5.0]}}"

# Nothing is happening yet because we have to put the drone to the "OFFBOARD" mode
# and arm it. In a new shell run the following commands:
rosservice call /mavros/set_mode "base_mode: 0
custom_mode: 'OFFBOARD'"

rosservice call /mavros/cmd/arming "value: true"

# Now stop publishing data (ctrl+c) and you will see that the drone will come back and land. 
# This is a safety measure. When there is no message received for more than 0.5 second in
# OFFBOARD mode, the drone will assume that there is something wrong with the computer (maybe
# crashed) and it will return to where it started and land.
\end{lstlisting}

\section{Reflecting back to how all of the components communicate}

As we explained in section~\ref{sec:components-in-a-simulated-environment} and saw in practice in section~\ref{sec:run-first-simulation} after we proceeded with the first simulation, there are many components involved. After you have started all of the different components as described in section~\ref{sec:run-first-simulation}, try to execute in a new shell the command \code{ss -ulp4}. This command will show you all of the IPv4 UDP ports that are opened in listening mode in the simulation server, and by which application these ports have been opened. You should get an output similar to this one:

\begin{lstlisting}[style=bash]
$ ss -ulp4
State      Recv-Q Send-Q        Local Address:Port                         Peer Address:Port                
UNCONN     0      0                         *:14540                                   *:*                     users:(("mavros_node",pid=28659,fd=15))
UNCONN     0      0                         *:14550                                   *:*                     users:(("QGroundControl",pid=29116,fd=36))
UNCONN     0      0                         *:14556                                   *:*                     users:(("px4",pid=28424,fd=6))
UNCONN     0      0                         *:14557                                   *:*                     users:(("px4",pid=28424,fd=7))
UNCONN     0      0                         *:14560                                   *:*                     users:(("px4",pid=28424,fd=5))
UNCONN     0      0                         *:33015                                   *:*                     users:(("gzserver",pid=28111,fd=41))
UNCONN     0      0                         *:58792                                   *:*                     users:(("mavros_node",pid=28659,fd=7))
UNCONN     0      0                         *:44820                                   *:*                     users:(("rosout",pid=27751,fd=7))
UNCONN     0      0                         *:53318                                   *:*                     users:(("gzserver",pid=28111,fd=13))
\end{lstlisting}


If you check the output of the \code{ss} command carefully, you can spot the different default communication ports that we described in Figure~\ref{fig:udp-port-communnication}.

\begin{itemize}
	\item \blue{PX4} has opened three UDP ports for sending/listening: \code{14556}, \code{14557} and \code{14560}. With these ports, PX4 communicates with the GCS, offboard computer and the simulator respectively.
	\item \blue{MAVROS} is listening to the port \code{14540} where \blue{PX4} is sending data from the port \code{14557} and vice versa, i.e. this communication is bidirectional and MAVROS is also using the port \code{14540} to send data to PX4 in the port \code{14557}.
	\item \blue{QGroundControl} is listening to the port \code{14550} where \blue{PX4} is sending data from the port \code{14556} and vice versa.
	\item \blue{Gazebo simulator} (\code{gzserver}) is listening to the port \code{33015}\footnote{This port is randomly chosen each time the simulator starts as reflected in Figure~\ref{fig:udp-port-communnication} as well, so expect to see a different port when you run the \code{{\footnotesize ss}} command.}\footnote{Note that there is one more UDP port opened by the \code{{\footnotesize gzserver}} process. The port \code{{\footnotesize 53318}} in this case. I do not know what is gazebo using another UDP port for, but you can find which of these two ports is used for the communication with PX4 by using this tcpdump command:\code{{\footnotesize sudo tcpdump -i any -n -c1 'udp and port 14560'}}. You will get an output that looks like this: \code{{\footnotesize 17:19:14.994361 IP 127.0.0.1.33015 > 127.0.0.1.14560: UDP, length 72}}} where \blue{PX4} is sending data from the port \code{14560} and vice versa.
\end{itemize}

