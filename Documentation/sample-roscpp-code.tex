\chapter{Sample ROS C++ code}

Now that we know how to simulate one or more drones and control them with ROS from the command line, it is time to prepare our first program. I will emphasize again, that if you haven't been through the basic ROS tutorials, do that now. We only use ROS from now on.

Let's create a ROS workspace for our program. We will call it \code{drone\_ws}:
\begin{lstlisting}[style=bash]
mkdir -p ~/drone_ws/src
cd ~/drone_ws/
catkin_make
\end{lstlisting}

After you run \code{catkin\_make} you will see that a couple of extra folders have been generated: The \code{build} and \code{devel} folders.

\code{catkin\_make} is a tool for building code in a catkin workspace. \code{catkin\_make} follows the standard layout of a catkin workspace, as described in \href{http://www.ros.org/reps/rep-0128.html}{http://www.ros.org/reps/rep-0128.html}.

To create a \code{drone} package that depends on \code{std\_msgs}, \code{roscpp}, \code{rospy} and \code{mavros} packages, run the following commands from your newly created catkin workspace:

\begin{lstlisting}[style=bash]
cd ~/drone_ws/src
catkin_create_pkg drone std_msgs rospy roscpp mavros
\end{lstlisting}

At this point you may want to change some details for your package by editing the file \code{drone/package.xml} and your package should be already visible by the ROS filesystem commands (once you source the \code{setup.bash} file of your workspace first).

\begin{lstlisting}[style=bash]
# From anywhere in your filesystem try the following commands:
source ~/drone_ws/devel/setup.bash
rospack find drone
rosls drone
roscat drone package.xml
\end{lstlisting}

You can recompile with \code{catkin\_make} and see that the newly created \code{drone} package is discovered by your \code{drone\_ws} catkin workspace.

\begin{lstlisting}[style=bash]
cd ~/drone_ws/
catkin_make
\end{lstlisting}

Now let's put a c++ program into our drone package and compile it. Download the program from our repository:
\begin{lstlisting}[style=bash]
# Place the source code in the src folder of the drone package
cd ~/drone_ws/src/drone/src
wget --user=vangelis@tasoulas.net --ask-password https://bitbucket.org/hioarobotics/multidrone-simulation/raw/efc0693bdf7146240e0c384bbba75c29f92de7df/sample-cpp-ros-code/drone_cpp.cpp
\end{lstlisting}

Now edit the file \code{\mytilde/drone\_ws/src/drone/CMakeLists.txt} and change the line:
\begin{lstlisting}[style=bash]
# add_executable(${PROJECT_NAME}_node src/drone_node.cpp)
\end{lstlisting}
to:

\begin{lstlisting}[style=bash]
add_executable(${PROJECT_NAME}_node_cpp src/drone_cpp.cpp)
\end{lstlisting}

Also change the following lines in the same file:
\begin{lstlisting}[style=bash]
# target_link_libraries(${PROJECT_NAME}_node
#   ${catkin_LIBRARIES}
# )
\end{lstlisting}

to:
\begin{lstlisting}[style=bash]
target_link_libraries(${PROJECT_NAME}_node_cpp
  ${catkin_LIBRARIES}
)
\end{lstlisting}

Try to compile with \code{catkin\_make}:
\begin{lstlisting}[style=bash]
cd ~/drone_ws
catkin_make
\end{lstlisting}

Check that the executable \code{drone\_node\_cpp} can be found by ROS. Get a list of the executables (nodes) for a specific package with the command \code{rospack-list-executables}\footnote{More details here: \href{https://stackoverflow.com/a/46673559/1275161}{https://stackoverflow.com/a/46673559/1275161}.}:
\begin{lstlisting}[style=bash]
cd ~/.local/bin/
wget --user=vangelis@tasoulas.net --ask-password https://bitbucket.org/hioarobotics/multidrone-simulation/raw/219c2d290bb62b29bbcf4446ce93380eb0cbc68b/scripts/rospack-list-executables
chmod +x rospack-list-executables
cd ~
complete -F _roscomplete rospack-list-executables
if ! grep 'rospack-list-executables' ~/.bashrc &>/dev/null; then
    echo "complete -F _roscomplete rospack-list-executables" >> ~/.bashrc
fi
rospack-list-executables drone
\end{lstlisting}

The last command should return the following:
\begin{lstlisting}[style=bash]
$ rospack-list-executables drone
drone_node_cpp
\end{lstlisting}

Now let's execute a simulation with two drones and the program we just compiled. I will not go into details about the programming part. I have added detailed comments in the code, so read it if you want to find out more. The program connects to one user-defined mavros node and one user-defined rplidar topic, and reads the data from the rplidar, as well as sets the drone in offboard mode and moves it slowly on the X axis (X with respect to the inertial navigation system of the drone).

\begin{lstlisting}[style=bash]
# In shell 1:
roscore

# In shell 2:
~/src/Firmware/Tools/sitl_run_gazebo_multidrone.sh 2

# In shell 3:
/opt/TurboVNC/bin/vncserver -list |
awk '{print $1}' | while read vncdisplay; do
if [[ ${vncdisplay:0:1} == ":" ]]; then
/opt/TurboVNC/bin/vncserver -kill ${vncdisplay}
fi
done
/opt/TurboVNC/bin/vncserver
export DISPLAY=:1
export PX4_HOME_LAT=59.916670
export PX4_HOME_LON=10.729153
export PX4_HOME_ALT=2.5
cd ~
source "$(pwd)/src/Firmware/Tools/setup_gazebo.bash" "$(pwd)/src/Firmware" "$(pwd)/src/Firmware/build/posix_sitl_default"
vglrun roslaunch gazebo_ros empty_world.launch world_name:="$(pwd)/src/Firmware/Tools/sitl_gazebo/worlds/2_drones.world"

# In shell 4:
cd ~/drone_ws/ && source ~/catkin_ws/devel/setup.bash && catkin_make && source ~/drone_ws/devel/setup.bash && rosrun drone drone_node_cpp __name:=drone_1 _mavros_node:=mavros_1

# In shell 5 (note that since we use the same ros node twice, we have to
# change the rosnode name with the __name:=<name> part):
cd ~/drone_ws/ && source ~/catkin_ws/devel/setup.bash && catkin_make && source ~/drone_ws/devel/setup.bash && rosrun drone drone_node_cpp __name:=drone_2 _mavros_node:=mavros_2

# In shell 6 check the available rosnodes:
$ rosnode list 
/drone_1   # This is our program controlling the first drone (name provided with the __name variable)
/drone_2   # This is our program controlling the second drone (name provided with the __name variable)
/gazebo
/mavros_1  # This is the mavros node that our program is using to communicate with the first drone (provided through the command line with the '_mavros_node:=mavros_1' arg in our program). The mavros nodes were started by the 'sitl_run_gazebo_multidrone.sh' script.
/mavros_2  # Similar here for the second drone
/rosout
\end{lstlisting}

Look both at the output of shell 4 and 5, as well as the Gazebo simulator to see what's happening. After a while, press \code{ctrl+C} in both shell 4 and 5 to stop the program. See what's happening (the drones have been programmed to land when the program stops). Reexecute the program with the \code{\_rplidar\_laser\_topic} arg to read data from the rplidar this time:

\begin{lstlisting}[style=bash]
# In shell 4:
cd ~/drone_ws/ && source ~/catkin_ws/devel/setup.bash && catkin_make && source ~/drone_ws/devel/setup.bash && rosrun drone drone_node_cpp __name:=drone_1 _mavros_node:=mavros_1 _rplidar_laser_topic:=laser_1/scan

# In shell 5 (note that since we use the same ros node twice, we have to
# change the rosnode name with the __name:=<name> part):
cd ~/drone_ws/ && source ~/catkin_ws/devel/setup.bash && catkin_make && source ~/drone_ws/devel/setup.bash && rosrun drone drone_node_cpp __name:=drone_2 _mavros_node:=mavros_2 _rplidar_laser_topic:=laser_2/scan
\end{lstlisting}

The program doesn't do anything with the data from the rplidar, but the sample code demonstrates how to read this data. Then it is up to the coder to use the data as they want.

You can check that the rplidar sensors actually read something by placing a large building close to the drone in the simulator as shown in Figures~\ref{fig:gazebo-building-1} and~\ref{fig:gazebo-building-2}.

\begin{figure}[t!]
	\centering
	\includegraphics[width=17cm]{figures/Gazebo-Office-Building-1.png}
	\caption{Click on the \textbf{Insert} tab in the Gazebo simulator window. Find the \textbf{Office Building}.}
	\label{fig:gazebo-building-1}
\end{figure}

\begin{figure}[t!]
	\centering
	\includegraphics[width=14cm]{figures/Gazebo-Office-Building-2.png}
	\caption{Place the building close to the drones as shown in this picture. Rerun the \textbf{drone\_node\_cpp} program and see the readings from the rplidar sensors in the shell output.}
	\label{fig:gazebo-building-2}
\end{figure}
