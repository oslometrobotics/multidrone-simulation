\chapter{Introduction}
The drones are complex machines, and in order to fly a drone there are several different components that needs to be put together. In this chapter we talk about the main components. I would suggest you to become very familiar with these different components before you jump to the simulation setup and experimentation in the next chapters. Note that especially the ROS tutorials (you will see when you start reading what I'm talking about) are a must if you want to understand what you are doing when you start communicating with the drone by sending low-level commands.

\section{Different components that are involved when building/flying a drone}
\label{sec:dronecomponents}

\paragraph{Flight controller:} The flight controller is the hardware that flies the drone. It is essentially the hardware that is responsible to control the rotational speed (RPMs) for each of the propellers in order to make the drone fly as expected. Different flight controllers exist such as the \textbf{Navio2}, \textbf{Pixhawk} and the \textbf{Intel Aero Flight Controller}. Some of the input sensors such as the GPS are usually directly connected to the flight controller hardware.

\paragraph{Autopilot software:} The autopilot software (or firmware) needs to be loaded into a flight controller to control the drone. The autopilot software receives commands with the MAVLink protocol and controls the flying behavior. Prebuilt flight modes allow autopiloting of the drone. \textbf{PX4} and \textbf{Ardupilot} are the two most popular open source autopilot software solutions. We use PX4\footnote{\href{http://px4.io}{http://px4.io}, \href{https://dev.px4.io/en/}{https://dev.px4.io/en/}} in this project.

\paragraph{MAVLink:} MAVLink\footnote{\href{http://qgroundcontrol.org/mavlink/start}{http://qgroundcontrol.org/mavlink/start}, \href{http://mavlink.org/messages/common}{http://mavlink.org/messages/common}} is a well defined communication protocol for drones. The autopilot software \textit{understands} and transmits commands that have been marshalled with the MAVLink protocol.

\paragraph{Transmitter/Receiver:} When flying the drone with a remote control an additional transmitter/receiver\footnote{\href{http://ardupilot.org/copter/docs/common-pixhawk-and-px4-compatible-rc-transmitter-and-receiver-systems.html}{http://ardupilot.org/copter/docs/common-pixhawk-and-px4-compatible-rc-transmitter-and-receiver-systems.html}} must be connected to the flight controller in order to send/receive MAVLink commands to control the drone in real time. Note that a transmitter/receiver is not needed for the simulation.

\paragraph{Compute Board:} If you want to add logic to the drone you must first write a program that reads sensor data from the drone, and sends the necessary commands to alter the drone behavior as needed. This software can be executed in your laptop, and send the commands over a WiFi link. Unfortunately, if the WiFi reception is poor (obstacles, the drone is out of reach etc), the drone will not be able to receive commands from our software anymore. In order to solve that problem, a small but powerful Linux-based compute board such as a Raspberry Pi or the Intel Aero Compute Board is typically added on the drone. Then we load the software in the compute board, and the compute board is wired to the flight controller and communicates with the autopilot software without interruptions even if the drone is out of sight or WiFi reach.

\paragraph{ROS:} ROS\footnote{\href{http://www.ros.org}{http://www.ros.org}, \href{http://wiki.ros.org/ROS/Tutorials}{http://wiki.ros.org/ROS/Tutorials}} is a robotics operating system. Virtually every robotics project I found online uses ROS in one way or another, and drones are no exception. Think of ROS as a collection of tools that helps you to program your robots with well defined interfaces and programming APIs. ROS is the interface between you and your robot. Since we will use ROS to program and send commands to our drones, it is necessary to understand common ROS components and terminology. Things you need to become familiar in order to proceed include:

\begin{itemize}
	\item ROS packages (\href{http://wiki.ros.org/Packages}{http://wiki.ros.org/Packages})
	\item ROS topics (\href{http://wiki.ros.org/Topics}{http://wiki.ros.org/Topics})
	\item ROS services (\href{http://wiki.ros.org/Services}{http://wiki.ros.org/Services})
	\item ROS virtual file system and \code{rosbash} (\href{http://wiki.ros.org/rosbash}{http://wiki.ros.org/rosbash})
	\item ROS nodes (\href{http://wiki.ros.org/Nodes}{http://wiki.ros.org/Nodes})
	\item ROS messages (\href{http://wiki.ros.org/Messages}{http://wiki.ros.org/Messages})
	\item ROS build system - \code{catkin} (\href{http://wiki.ros.org/catkin/conceptual_overview}{http://wiki.ros.org/catkin/conceptual\_overview})
\end{itemize}

The easiest and quickest way I found to understand the basics of ROS was to follow the beginner ROS tutorials (at least from 1 to 10): \href{http://wiki.ros.org/ROS/Tutorials}{http://wiki.ros.org/ROS/Tutorials}

I would suggest you to go through these ROS tutorials now before proceeding any further.

\paragraph{MAVROS:} MAVROS\footnote{\href{http://wiki.ros.org/mavros}{http://wiki.ros.org/mavros}} is a ROS package that allows us to send/receive MAVLink commands to our drones in the \textit{ROS way}. If we use MAVROS, we do not have to worry about the marshalling/unmarshalling of the MAVLink messages we want to send to the autopilot software. We can use the well defined ROS APIs and programming style, and the MAVROS package will take care of the underlying complexity.

\paragraph{Gazebo Simulator:} Gazebo\footnote{\href{http://gazebosim.org}{http://gazebosim.org}} is a 3D robotics simulator. Gazebo tries to implement a \textit{real} world for your drones to interact with. It provides a physics engine, programmatic and graphical interfaces that allows you to implement real worlds. Several gazebo plugins exist that model different behavior (wind, wifi transmitters and receivers etc). If a required behavior doesn't exist, a plugin has to be created in order to simulate additional functionality.

\paragraph{QGroundControl:} QGroundControl\footnote{\href{http://qgroundcontrol.com}{http://qgroundcontrol.com}} provides a GUI software to act as a \textit{Ground Control Station}. QGroundControl provides full flight control and mission planning. Essentially, QGroundControl gives you a nice point-and-click GUI to send MAVLink commands to and read data from your drone.

\section{Putting all the components together}

\subsection{In a real-world setup}

Figure~\ref{fig:connecting-everything-realworld} tries to visualize how the different components described in section~\ref{sec:dronecomponents} are connected and communicating in a real-world setup.

\begin{enumerate}
	\item A flight controller board and a compute board are both mounted on a drone frame. Note that the transmitter/receiver is also connected to the flight controller, but it is not depicted in this figure as this is a component that we do not bother using in the simulations (althought it is possible to use if needed).
	\item The flight controller, which typically runs a real-time operating system (RTOS) providing a POSIX-style environment (i.e. \code{printf()}, \code{pthreads}, \code{/dev/ttyS1}, \code{open()}, \code{write()}, \code{poll()}, \code{ioctl()}, etc), executes the autopilot software. The autopilot software then exchanges information (reads the sensor inputs and control the propellers) with the flight controller as shown by the arrow No \circled{4}. The mavlink protocol is used for that communication.
	\item The compute board runs a Linux-based OS that has ROS, MAVROS and our program installed.
	\item Our program, that is responsible to add the extra logic to the drone, is a piece of software that has been written in C++/Python code that uses the ROS API and the MAVROS package as the arrows No \circled{1} and No \circled{2} show.
	\item Eventually, MAVROS is marshalling the information using the MAVLink protocol, and communicates with the autopilot software that is located in the flight controller (arrow No \circled{3}). This communication is happening either with a serial (ttySX) interface or UDP sockets. Note that the communication between the autopilot software and MAVROS is bidirectional, meaning that our program can access data that are collected from the flight controller sensors and take decisions accordingly.
\end{enumerate}

\begin{figure}[t!]
	\centering
	\includegraphics[width=10cm]{figures/Connecting-Everything-RealWorld.pdf}
	\caption{Block diagram that shows how the different components described in section~\ref{sec:dronecomponents} are connected and communicating in a real-world setup.}
	\label{fig:connecting-everything-realworld}
\end{figure}

\subsection{In a simulated environment}
\label{sec:components-in-a-simulated-environment}

\begin{figure}[t!]
	\centering
	\includegraphics[width=10cm]{figures/Connecting-Everything-SimulatedWorld.pdf}
	\caption{Block diagram that shows how the different components described in section~\ref{sec:dronecomponents} are connected and communicating in a simulated setup.}
	\label{fig:connecting-everything-simulatedworld}
\end{figure}

Figure~\ref{fig:connecting-everything-simulatedworld} tries to visualize how the different components described in section~\ref{sec:dronecomponents} are connected and communicating in a simulated setup. Figure~\ref{fig:connecting-everything-simulatedworld} uses the same colors as Figure~\ref{fig:connecting-everything-realworld} to pinpoint the equivalent components. The word \textit{equivalent} is used here because some of the components that exist in a real world setup (e.g. the flight controller) are fictional in the simulated setup and/or are implemented as part of another component.

\begin{enumerate}
	\item Gazebo simulator is used to simulate our real world (obstacles, wind, the drone itself etc).
	\item The drone itself is modeled in gazebo, and in this model (in the definition file of the model) is where the UDP port where the simulator will be communicating with the autopilot software (PX4) is defined. The default port used in all the gazebo models is the port defined as default by the MAVLink protocol for simulation purposes: the port \code{14560}. In particular, PX4 defines three standard UDP broadcast ports that ground control stations (e.g. QGroundControl), Offboard APIs (like MAVROS) and simulators (like Gazebo) can use to communicate with the autopilot.
	\begin{itemize}
		\item Default Offboard APIs port: 14540
		\item Default Ground Control Stations port: 14550 (QGroundControl automatically connects to PX4 broadcasting on this port).
		\item Default Simulator port: 14560
	\end{itemize}
	For a visual representation of the UDP communication, please look at the Figure~\ref{fig:udp-port-communnication}\footnote{Read more details in this link: \href{https://github.com/PX4/Devguide/tree/master/en/simulation}{https://github.com/PX4/Devguide/tree/master/en/simulation}}.
	\item Then we need to execute an instance of the autopilot software and configure the instance to open up two ports for communication/broadcasting: One port has to be the port that is defined in the gazebo drone model (\code{14560} by default), and a second UDP port will be used for the communication with MAVROS (\code{14540} is the default port that is a commonly used port). Note that by default a third port is opened (\code{15550}) in order to allow QGroundControl to connect as well.
	\item Then we have to run a MAVROS instance that has to be configured to listen to port \code{14540} (or any other port that we have configured in the previous step).
	\item At this point we can execute MAVROS commands via the ROS command line interface and we should see the 3D drone moving in the simulator.
	\item If we now execute our software, it should be able to control the drone via MAVROS.
	\item Read the next section for a step by step guide on how to install of the necessary components for the simulation, and how to execute some basic MAVROS commands.
\end{enumerate}

\begin{figure}[t!]
	\centering
	\includegraphics[width=12cm]{figures/px4_sitl_overview.pdf}
	\caption{Default UDP MAVLink port communication. SITL stands for \textit{Software In The Loop.}}
	\label{fig:udp-port-communnication}
\end{figure}