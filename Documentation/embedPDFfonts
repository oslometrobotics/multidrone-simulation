#!/bin/bash

if [[ $# -ne 1 ]]; then
	echo "Please provide a PDF file as a first argument, to embed the all fonts to it."
	exit 1
fi

which pdftk > /dev/null
exit_status=$?
if [[ $exit_status -ne 0 ]]; then "pdftk command was not found in the known PATH. Please install pdftk and run this script again."; exit 1; fi
which gs > /dev/null
exit_status=$?
if [[ $exit_status -ne 0 ]]; then "gs (GhostScript) command was not found in the known PATH. Please install gs and run this script again."; exit 1; fi

input_filename="$1"
metadata_file=$(mktemp)

# Save metadata
# http://unix.stackexchange.com/questions/50475/how-to-make-ghostscript-not-wipe-pdf-metadata
pdftk "$input_filename" dump_data_utf8 > "$metadata_file"

extension=$(echo "$input_filename" | rev | colrm 5 | rev)
filename=$( echo "$input_filename" | colrm $(( ${#input_filename} - 3 )) )
extension_lowercase=${extension,,}

output_file="$filename"-FontsEmbedded"$extension"
tmp_output_file=$(mktemp).pdf

if [[ "$extension_lowercase" == ".pdf" ]]; then
	# http://stackoverflow.com/questions/13912615/how-can-one-embed-a-font-into-a-pdf-with-free-linux-command-line-tools
	gs -q \
		-dNOPAUSE \
		-dBATCH \
		-dPDFSETTINGS=/prepress \
		-sDEVICE=pdfwrite \
		-sOutputFile="$tmp_output_file" \
		"$input_filename"
else
	echo "The file needs to have a pdf extension."
	exit 1
fi

echo "Do you want to overwrite the input file ("$input_filename")? (y/N) "
read input

# Restore metadata
pdftk "$tmp_output_file" update_info_utf8 "$metadata_file" output "$output_file"

rm "$metadata_file" "$tmp_output_file"

if [[ ${input,,} == "y" || ${input,,} == "yes" ]]; then
	mv "$output_file" "$input_filename"
fi
