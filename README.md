Setup and program PX4, ROS and Gazebo for multi-drone (quad-copter) simulations.

### How to compile the documentation

* `git clone git@bitbucket.org:hioarobotics/multidrone-simulation.git`
* `cd multidrone-simulation/Documentation`
* `./compile-doc`
* `xdg-open documentation.pdf`

Note that I have paid particular attention to make the code blocks in the pdf document copy/paste-able, so while you are reading you can also test things in a terminal by copy/pasting the commands that you see.

### The documentation will guide you through:
* The different components that are involved in a drone-building and programming process. This will help you to get started.
* The Installation of the different components.
* Your first simulation with one drone that will allow you to control the drone with basic commands from the command line.
* Detailed steps about what you need to do in order to simulate more than one drone.
* The basics to get you started with programming your drones.

### Scripts and sample code
There are a few scripts that have been created in order to ease some of the very cumbersome tasks when simulating drones.

The scripts are:

* `sitl_px4_run.sh`: This script will perform some sanity checks and start a single PX4 instance in accordance with the documentation
* `sitl_run_gazebo_multidrone.sh`: This script is much more complex, and it will start multiple PX4 instances, and configure the Gazebo models as needed to start a simulation with several drones. The script will also execute the necessary `mavros` commands to start one `mavros` instance for controlling each drone. Then the only thing you have to do is to start the Gazebo simulator and run commands or your program to start controlling the simulated drone. You can execute this script by passing as a parameter the number of drones that you want to simulate.
* `rospack-list-executables`: A script that lists all of the binaries in a ros packages

Sample code:
* `drone_cpp.cpp`: Follow the documentation to understand how to compile this ROS-based program. The program will control one drone via `mavros`, and read data from an rplidar ROS topic. It will also move the drone in one direction. The source code is very well documented in order to get someone easily started.