#!/bin/bash
# Author: Vangelis Tasoulas (vangelis@tasoulas.net)
#
# run multiple instances of the 'px4' binary, but w/o starting the simulator.

num_drones=2

# If the user provided a positive integer, take it into account.
# Otherwise use the default number of drones in the simulaton as
# provided in the num_drones variable above.
if [[ $# -gt 0 ]]; then
    [[ "$1" -ge 0 ]] 2> /dev/null;
    if [[ "$?" == 0 ]]; then
        num_drones="$1"
    else
        if [[ "$1" != "k" ]]; then
            echo "Not a valid positive integer provided (or 'k' to kill"
            echo "all running instances). PX4 will not be started."
            exit 1
        fi
    fi
fi

# Start with the default ports, and increment all the ports by 30.
offboard_remote_port=14540
offboard_px4_port=14557
gcs_remote_port=14550
gcs_px4_port=14556
sim_port=14560
port_step=30

# Find the necessary dirs
script_dir="$( cd "$( dirname "$0" )" && pwd )"
src_path="$(cd "${script_dir}/.." && pwd)"
px4_bin="${src_path}/build/posix_sitl_default/px4"
eval base_instance_dir="~/px4_instances"
working_dir="${base_instance_dir}/px4_instance"

# The Gazebo units distance between the drones (in X axis)
distance_between_drones=4.5

# The RC template. Gzipped/base64 encoded.
rc_script_template_b64="H4sIAI/OEFoAA5WW0XaiMBCG73mKvICKtHV3LyMgcgqEDbFbezMnVdbjWSAsYLd9+x3AVmC17Xpl
5s+XTP5JRg+qeCRlJYtKy2UhU5IoudW2spKpzHpCGVdkTgUEYNqeF5GrTtykHlDT1MG1yPTqy8yY
XZ9T79liQfSxPj0rRvjVJtML8rpmR5fg9fvww3sbP7zHTtsjTfUvxtdz6qUjOWvOGjsM4xsaMmRb
+RLsU6e18ttsNp2dE8+SzAfLjSj3waOB1S8Qamxe5yyIcSbOzUbSh9ICPBZFIMjNQEHADcBnFhrX
kezbhQHUtTDL6PaMEjg250i6Yph9oztzl0aX5aUj2i31oYKmgFiH/WR8etcGuyf2TQhdYS4hJLMz
YU6FjZI+HjCced6/SB09T7hoGr21sUxAPfR83DXQD01YMq926R4eUNQH4gPc2R64uOh0yLVSvV83
lwCPivcROLUGy9WK5f1beS48sOzItAOryfCmR9VqfYlwikfXverXErfFigcNd6X3wMgOIpgzyi00
p3+hOtI9HO9v/emWLFpHQFeCRYJyQa71qT4Q0XQ7EuBwtgp756lFbjfca9GLOE/kC6mKF5nnyUsz
t9TKfXpIZKWKtruRUUlGBwKR64cMWa1SWQwykUV6bH+7l0IhdBzJzSZOTsNH2RO3m9Ngl5enQf4n
BXWooI6kahsDBrQyzkpVlMcpG5Viz93Gx8S0BAewjat4c0oWU6/2G5VXcaFl8mm/Ox1Ei3/9NI5f
0w3kqoSNyqpCJaegrKphcP+MG9ZNn0y28dPkmGd+qHTCmb+IJvnz9c/0AHV2Kps088vJ74PE3Dbj
VO6zMca0VD4l++zXq6fPjaf4ABtPyajAWjYfMlJNnLVmv4cZfS4lKntUstj2ljC6axQxXgZkbvS6
qiGLXOGyAB8id2yBnaxun4Ft9XK7iLfT3xb5NOd4bN4F3UB8DqRCuGKF3e2/ZsP3FTYgHuBW/wm2
tnwOimx+x4CtRLgS2GR+gP4RZzQc/kyYSxoENv5h+Ahod1q6zhLfMbj+6iNi2gAsFG5dqYXHfjT9
r0clareL3556TEbV20KPStWvIc0TfGOdZtE+jL+Q7zbkGQkAAA=="

# Define the Gazebo model dirs that we want to copy and change in order to create
# additional drones for our simulation.
eval iris_model_dir="${src_path}/Tools/sitl_gazebo/models/iris"
eval iris_rplidar_model_dir="${src_path}/Tools/sitl_gazebo/models/iris_rplidar"
eval lidar_model_dir="${src_path}/Tools/sitl_gazebo/models/lidar"
eval rplidar_model_dir="${src_path}/Tools/sitl_gazebo/models/rplidar"
eval flow_cam_model_dir="${src_path}/Tools/sitl_gazebo/models/flow_cam"

if [[ ! -x "${px4_bin}" ]]; then
    echo "px4 binary was not found. Building source code with 'make posix_sitl_default'"
    cd "$src_path"
    make posix_sitl_default
fi

if [[ ! -f "${iris_model_dir}/iris.sdf" ]]; then
    echo "It seems that you don't have the necessary Gazebo models in place."
    echo "Will try to fetch them now by issuing the command"
    echo "                          make posix_sitl_default sitl_gazebo"
    cd "$src_path"
    make posix_sitl_default sitl_gazebo
fi

echo "killing all px4 instances"
pkill -x px4 || true

echo "killing all mavros_node instances"
killall -9 mavros_node 2> /dev/null

# Since we are going to delete the instance directories, ensure that there
# issue with the ${working_dir} variable. Otherwise unexpected things can
# happen (and we do not want that).
mkdir -p "${base_instance_dir}"
if [[ ! -z "$(dirname ${working_dir})" && "${base_instance_dir}" == "$(dirname ${working_dir})" ]]; then
    find "${base_instance_dir}" -mindepth 1 -maxdepth 1 -type d -name "$(basename ${working_dir})*" -print -exec rm -rf {} \; | \
    while read line; do
        echo "Removing directory ${line}"
    done
else
    echo "Did you choose a correct base_instance_dir and working_dir?"
    exit 1
fi

# If the user gave a 'k' argument, just kill all px4 and do not start a new instance. Stop here.
if [[ "$1" == k ]]; then exit 0; fi

sleep 1

function port_in_use() {
    port=$1
    ss -upnl | awk '{print $4}' | grep :${port}$ &> /dev/null
    return $?
}

function ports_in_use() {
    if port_in_use ${sim_port}; then return 0;
    elif port_in_use ${gcs_px4_port}; then return 0;
    elif port_in_use ${gcs_remote_port}; then return 0;
    elif port_in_use ${offboard_px4_port}; then return 0;
    elif port_in_use ${offboard_px4_port}; then return 0;
    else return 1; fi
}

function increase_ports () {
    ((sim_port += port_step))
    ((gcs_px4_port += port_step))
    ((gcs_remote_port += port_step))
    ((offboard_px4_port += port_step))
    ((offboard_remote_port += port_step))
}

user=`whoami`

for ((i=1; i<=${num_drones}; ++i)); do
    # First ensure that the ports are not used already.
    while ports_in_use; do
        # If they are used, increase the port numbers.
        echo ${sim_port}
	increase_ports
    done

    # Then generate a unique directory name and create it
    cwd="${working_dir}_${i}"
    mkdir -p "${cwd}"
    # Get into it
    cd "${cwd}"
    # Replace template config with configured ports of current instance
    echo "${rc_script_template_b64}" | base64 -d | gunzip > px4_rc
    sed -i s/_SIMPORT_/${sim_port}/ px4_rc
    sed -i s/_MAVPORT_/${gcs_px4_port}/g px4_rc
    sed -i s/_MAVOPORT_/${gcs_remote_port}/ px4_rc
    sed -i s/_MAVPORT2_/${offboard_px4_port}/ px4_rc
    sed -i s/_MAVOPORT2_/${offboard_remote_port}/ px4_rc

    # Start PX4
    sudo -b -u ${user} "${px4_bin}" -d "${src_path}" px4_rc >out.log 2>err.log

    echo -e "---------"
    echo -e ""

    # Create additional drone models with rplidars
    echo "Creating a copy of the iris_rplidar drone model (${i})"
    rsync -avP "${iris_rplidar_model_dir}/" "${iris_rplidar_model_dir}_${i}/" > /dev/null
    cd "${iris_rplidar_model_dir}_${i}/"
    mv iris_rplidar.sdf iris_rplidar_${i}.sdf
    sed -i "s|<model name='iris_rplidar'>|<model name='iris_rplidar_"${i}"'>|" iris_rplidar_${i}.sdf

    # Tell the model.config which file describes our new model.
    sed -i 's|<sdf>iris_rplidar.sdf<|<sdf>iris_rplidar_'${i}'.sdf<|' model.config
    # Give the new model a slightly different name to differentiate it from the original.
    sed -i 's|<name>3DR Iris with rplidar lidar<|<name>3DR Iris with rplidar lidar ('${i}')<|' model.config

    # For the iris_rplidar_${i} you must load the iris_${i}, lidar_${i}, rplidar_{i} and flow_cam_{i} models that we
    # are going to create next.
    sed -i 's|<uri>model://iris<|<uri>model://iris_'${i}'<|' iris_rplidar_${i}.sdf
    sed -i 's|<uri>model://lidar<|<uri>model://lidar_'${i}'<|' iris_rplidar_${i}.sdf
    sed -i 's|<uri>model://rplidar<|<uri>model://rplidar_'${i}'<|' iris_rplidar_${i}.sdf
    sed -i 's|<uri>model://flow_cam<|<uri>model://flow_cam_'${i}'<|' iris_rplidar_${i}.sdf

    # Now create the iris_${i} model
    echo "   iris_rplidar_${i} depends on the iris model."
    echo "    Creating a copy of the iris drone model (${i})"
    rsync -avP "${iris_model_dir}/" "${iris_model_dir}_${i}/" > /dev/null
    cd "${iris_model_dir}_${i}/"
    mv iris.sdf iris_${i}.sdf
    # Change the communication port to ${sim_port}
    sed -i 's|<mavlink_udp_port>14560<|<mavlink_udp_port>'${sim_port}'<|' iris_${i}.sdf
    sed -i 's|<name>3DR Iris<|<name>3DR Iris ('${i}')<|' model.config
    sed -i "s|<sdf version='1.4'>iris.sdf<|<sdf version='1.4'>iris_"${i}".sdf<|" model.config

    # Then the lidar_{i} model
    echo "   iris_rplidar_${i} depends on the lidar model."
    echo "    Creating a copy of the lidar model (${i})"
    rsync -avP "${lidar_model_dir}/" "${lidar_model_dir}_${i}/" > /dev/null
    cd "${lidar_model_dir}_${i}/"
    sed -i 's|<sensor name="laser"|<sensor name="laser_'${i}'"|' model.sdf
    sed -i 's|<visual name="visual"|<visual name="visual_'${i}'"|' model.sdf

    # Then the rplidar_{i} model
    echo "   iris_rplidar_${i} depends on the rplidar model."
    echo "    Creating a copy of the rplidar model (${i})"
    rsync -avP "${rplidar_model_dir}/" "${rplidar_model_dir}_${i}/" > /dev/null
    cd "${rplidar_model_dir}_${i}/"
    sed -i 's|<topicName>laser/scan<|<topicName>laser_'${i}'/scan<|' model.sdf
    sed -i 's|<frameName>rplidar_link<|<frameName>rplidar_link_'${i}'<|' model.sdf
    sed -i 's|<sensor name="laser"|<sensor name="laser_'${i}'"|' model.sdf
    sed -i 's|<visual name="visual"|<visual name="visual_'${i}'"|' model.sdf

    # Finish with the flow_cam_{i} model
    echo "   iris_rplidar_${i} depends on the flow_cam model."
    echo "    Creating a copy of the flow_cam model (${i})"
    rsync -avP "${flow_cam_model_dir}/" "${flow_cam_model_dir}_${i}/" > /dev/null
    cd "${flow_cam_model_dir}_${i}/"
    sed -i 's|<topic>/px4flow<|<topic>px4flow_'${i}'<|' model.sdf
    sed -i 's|<sensor name="PX4Flow"|<sensor name="PX4Flow_'${i}'"|' model.sdf

    # You though it was so easy? Well, you were wrong. There is one last thing we need to do.
    # Create mavros launch files that will spawn mavros instances with different node names.
    echo "Creating mavros launch files"
    rsync -avP ~/catkin_ws/src/mavros/mavros/launch/node.launch ~/catkin_ws/src/mavros/mavros/launch/node_${i}.launch > /dev/null
    sed -i 's|name="mavros"|name="mavros_'${i}'"|' ~/catkin_ws/src/mavros/mavros/launch/node_${i}.launch
    rsync -avP ~/catkin_ws/src/mavros/mavros/launch/px4.launch ~/catkin_ws/src/mavros/mavros/launch/px4_${i}.launch > /dev/null
    sed -i 's|launch/node.launch">|launch/node_'${i}'.launch">|' ~/catkin_ws/src/mavros/mavros/launch/px4_${i}.launch

    # Print some info to the user
    echo -e ""
    echo -e "Starting \033[0;31minstance $i\033[0m in \033[0;32m${cwd}\033[0m"
    echo -e "PX4 will be broadcasting data to the:"
    echo -e "   * Offboard API (MAVROS) from the src port \033[1;33m'$offboard_px4_port'\033[0m to the dst port \033[1;33m'$offboard_remote_port'\033[0m"
    echo -e "   * QGroundControl from the src port \033[1;33m'$gcs_px4_port'\033[0m to the dst port \033[1;33m'$gcs_remote_port'\033[0m"
    echo -e "   * Simulator from a random src port to the dst port \033[1;33m'$sim_port'\033[0m"
    echo -e ""
    echo -e "The following 'roslaunch mavros' command will now be executed:"
    echo -e "    \033[1;37mroslaunch mavros px4_${i}.launch fcu_url:=\"udp://:$offboard_remote_port@127.0.0.1:$offboard_px4_port\"\033[0m"

    cd "${cwd}"
    nohup roslaunch mavros px4_${i}.launch fcu_url:="udp://:$offboard_remote_port@127.0.0.1:$offboard_px4_port" > mavros.log 2> mavros.err.log &
    sleep 1
    echo -e ""
    echo -e "You can find the mavros_${i} stdout and stderr in the files ${cwd}/mavros.log and ${cwd}/mavros.err.log"
    echo -e "For the full logging please check '$(ps xau | grep name:=mavros_${i} | grep -v grep | rev | cut -d= -f1 | rev)'"

    # Increase the port numbers
    increase_ports
done

echo ""
echo "Creating a Gazebo world with ${num_drones} models"
eval worldpath="~/src/Firmware/Tools/sitl_gazebo/worlds"
myworld="${worldpath}/${num_drones}_drones.world"

# Copy the iris.world template
rsync -avP ~/src/Firmware/Tools/sitl_gazebo/worlds/iris.world "${myworld}" > /dev/null

# We need xmlstarlet for the rest of the script. Check if it is already installed,
# in order to avoid nagging the user for their sudo password all the time.
if ! dpkg-query -W xmlstarlet &> /dev/null; then 
    echo "We need xmlstarlet to proceed. Please input your sudo password to install that package"
    sudo apt-get install xmlstarlet
fi

# Delete the existing iris model (it's the last non-commented include element).
xmlstarlet ed --inplace -d '/sdf/world[@name="default"]/include[last()]' "${myworld}"

# Use the asphalt ground
xmlstarlet ed --inplace \
          -i '/sdf/world[@name="default"]/physics[@name="default_physics"]' -t elem -name "include" -v "" \
          -s '/sdf/world[@name="default"]/include[last()]' -t elem -name "uri" -v "model://asphalt_plane" \
          "${myworld}"
 
# Add new include elements iris_rplidar drones.
for ((i=1; i<=${num_drones}; ++i)); do
    xmlstarlet ed --inplace \
          -i '/sdf/world[@name="default"]/physics[@name="default_physics"]' -t elem -name "include" -v "" \
          -s '/sdf/world[@name="default"]/include[last()]' -t elem -name "uri" -v "model://iris_rplidar_${i}" \
          -s '/sdf/world[@name="default"]/include[last()]' -t elem -name "pose" -v "$(echo | awk "{print ${distance_between_drones} * (${i} - 1)}") 0 0 0 0 0" \
          "${myworld}"
done

echo "You can now execute the simulator with the following set of commands:"
echo ""
echo "export PX4_HOME_LAT=59.916670
export PX4_HOME_LON=10.729153
export PX4_HOME_ALT=2.5
source ${src_path}/Tools/setup_gazebo.bash ${src_path} ${src_path}/build/posix_sitl_default
vglrun roslaunch gazebo_ros empty_world.launch world_name:=${myworld}"
