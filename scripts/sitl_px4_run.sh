#!/bin/bash
# Author: Vangelis Tasoulas (vangelis@tasoulas.net)
#
# run one instance of the 'px4' binary, but w/o starting the simulator.
# It assumes px4 is already built, with 'make posix_sitl_default'

sim_port=14560
mav_port=14556
mav_port2=14557

mav_oport=14550
mav_oport2=14540

eval px4_bin="~/src/Firmware/build/posix_sitl_default/px4"
eval working_dir="~/px4-instance"
eval src_path="~/src/Firmware"
rc_script_template_b64="H4sIAI/OEFoAA5WW0XaiMBCG73mKvICKtHV3LyMgcgqEDbFbezMnVdbjWSAsYLd9+x3AVmC17Xpl
5s+XTP5JRg+qeCRlJYtKy2UhU5IoudW2spKpzHpCGVdkTgUEYNqeF5GrTtykHlDT1MG1yPTqy8yY
XZ9T79liQfSxPj0rRvjVJtML8rpmR5fg9fvww3sbP7zHTtsjTfUvxtdz6qUjOWvOGjsM4xsaMmRb
+RLsU6e18ttsNp2dE8+SzAfLjSj3waOB1S8Qamxe5yyIcSbOzUbSh9ICPBZFIMjNQEHADcBnFhrX
kezbhQHUtTDL6PaMEjg250i6Yph9oztzl0aX5aUj2i31oYKmgFiH/WR8etcGuyf2TQhdYS4hJLMz
YU6FjZI+HjCced6/SB09T7hoGr21sUxAPfR83DXQD01YMq926R4eUNQH4gPc2R64uOh0yLVSvV83
lwCPivcROLUGy9WK5f1beS48sOzItAOryfCmR9VqfYlwikfXverXErfFigcNd6X3wMgOIpgzyi00
p3+hOtI9HO9v/emWLFpHQFeCRYJyQa71qT4Q0XQ7EuBwtgp756lFbjfca9GLOE/kC6mKF5nnyUsz
t9TKfXpIZKWKtruRUUlGBwKR64cMWa1SWQwykUV6bH+7l0IhdBzJzSZOTsNH2RO3m9Ngl5enQf4n
BXWooI6kahsDBrQyzkpVlMcpG5Viz93Gx8S0BAewjat4c0oWU6/2G5VXcaFl8mm/Ox1Ei3/9NI5f
0w3kqoSNyqpCJaegrKphcP+MG9ZNn0y28dPkmGd+qHTCmb+IJvnz9c/0AHV2Kps088vJ74PE3Dbj
VO6zMca0VD4l++zXq6fPjaf4ABtPyajAWjYfMlJNnLVmv4cZfS4lKntUstj2ljC6axQxXgZkbvS6
qiGLXOGyAB8id2yBnaxun4Ft9XK7iLfT3xb5NOd4bN4F3UB8DqRCuGKF3e2/ZsP3FTYgHuBW/wm2
tnwOimx+x4CtRLgS2GR+gP4RZzQc/kyYSxoENv5h+Ahod1q6zhLfMbj+6iNi2gAsFG5dqYXHfjT9
r0clareL3556TEbV20KPStWvIc0TfGOdZtE+jL+Q7zbkGQkAAA=="

echo "killing running instances"
pkill -x px4 || true

rm -rf "${working_dir}" 2> /dev/null

# If the user gave a 'k' argument, just kill all px4 and do not start a new instance.
if [[ "$1" == k ]]; then exit 0; fi

sleep 1

user=`whoami`

mkdir -p "${working_dir}"
cd "${working_dir}"
# replace template config with configured ports of current instance
echo "${rc_script_template_b64}" | base64 -d | gunzip > px4_rc
sed -i s/_SIMPORT_/${sim_port}/ px4_rc
sed -i s/_MAVPORT_/${mav_port}/g px4_rc
sed -i s/_MAVOPORT_/${mav_oport}/ px4_rc
sed -i s/_MAVPORT2_/${mav_port2}/ px4_rc
sed -i s/_MAVOPORT2_/${mav_oport2}/ px4_rc

echo "Starting instance $n in $(pwd)"
echo "PX4 will be broadcasting data to the:"
echo "   * Offboard API (MAVROS) from the src port '$mav_port2' to the dst port '$mav_oport2'"
echo "   * QGroundControl from the src port '$mav_port' to the dst port '$mav_oport'"
echo "   * Simulator from a random src port to the dst port '$sim_port'"
echo ""
echo "You can now execute 'roslaunch mavros' command like this:"
echo "    roslaunch mavros px4.launch fcu_url:=\"udp://:$mav_oport2@127.0.0.1:$mav_port2\""
sudo -b -u ${user} "${px4_bin}" -d "${src_path}" px4_rc >out.log 2>err.log
