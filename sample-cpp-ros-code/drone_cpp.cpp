/**
 * Author: Vangelis Tasoulas (vangelis@tasoulas.net)
 *
 * # This initial code has been inspired from here:
 * https://dev.px4.io/en/ros/mavros_offboard.html
 *
 * # PX4 Flight modes and explanation for each flight mode:
 * http://wiki.ros.org/mavros/CustomModes#PX4_native_flight_stack
 * https://dev.px4.io/en/concept/flight_modes.html
 *
 * # Another simple code example:
 * http://docs.erlerobotics.com/simulation/vehicles/erle_copter/tutorial_3
 *
 * # Another slightly more advanced example:
 * https://404warehouse.net/2015/12/20/autopilot-offboard-control-using-mavros-package-on-ros/
 *
 * # Example Publish/Subscribe ROS programming:
 * http://wiki.ros.org/ROS/Tutorials/WritingPublisherSubscriber(c%2B%2B)
 */
#include <ros/ros.h>
#include <geometry_msgs/PoseStamped.h>
#include <mavros_msgs/CommandBool.h>
#include <mavros_msgs/CommandTOL.h>
#include <mavros_msgs/SetMode.h>
#include <mavros_msgs/State.h>
#include <mavros_msgs/ExtendedState.h>
#include <mavlink/v2.0/common/mavlink.h>
#include <sensor_msgs/LaserScan.h>
#include <signal.h>
#include <boost/filesystem.hpp>

/************************************
 * ----- Variable definitions ----- *
 ************************************/
mavros_msgs::State current_state;
mavros_msgs::ExtendedState current_extended_state;
sensor_msgs::LaserScan current_laserscan;

/* Signal-safe flag for whether shutdown is requested */
sig_atomic_t volatile request_shutdown_flag = 0;

/************************************
 * ----- Function definitions ----- *
 ************************************/
void usage(const char *);
void mySigIntHandler(int);
void state_cb(const mavros_msgs::State::ConstPtr&);
void extended_state_cb(const mavros_msgs::ExtendedState::ConstPtr&);
void laserscan_cb(const sensor_msgs::LaserScan::ConstPtr&);
int rosNodeExists(std::string);
int main(int, char**);

/**
 * ros::ok() becomes false once ros::shutdown() has been called and is finished.
 * Moreover, since I have implemented my own SIGINT handler, "ok" for my program
 * means ros::ok() && 'shutdown hasn't been requested'!.
 *
 * This function returns 1 when both of these conditions are met, otherwise it
 * returns 0.
 */
inline int rosok()
{
	return ros::ok() && !request_shutdown_flag;
}

/**
 * The get_rosnode_path() function simply returns a complete node path
 * for the given mavros_node.
 */
inline std::string getRosnodePath(std::string mavros_node, const char *path)
{
	return mavros_node + path;
}

/****************************************
 * ----- Function implementations ----- *
 ****************************************/

/**
 * Provide some help message
 */
void usage(const char *exec_path)
{
	/* Use the boost library to extract only the name of the executable easily */
	boost::filesystem::path b_exec_path(exec_path);

	printf("\n"
	       "Please provide the name of the mavros node that you want to communicate with.\n"
	       "Example usage:\n"
	       "    %s mavros_2\n"
	       "\n"
	       "Note that if you do not provide any parameter, the default 'mavros' node name\n"
	       "will be used.\n"
	       "\n", b_exec_path.filename().c_str());
}

/**
 * The mySigIntHandler overwrites the default SIGINT handler that is assigned
 * by ROS, changes a flag to let the user know that a shutdown has been requested.
 * The user then can write some pre-shutdown code before actually calling the
 * ros::shutdown() function.
 */
void mySigIntHandler(int sig)
{
	(void)sig;
	request_shutdown_flag = 1;
}

/**
 * The state_cb() function is a callback function that is registered with
 * a subscriber and updates the current state of the drone.
 */
void state_cb(const mavros_msgs::State::ConstPtr& msg)
{
	current_state = *msg;
}

/**
 * The extended_state_cb() function is a callback function that is registered
 * with a subscriber and updates the current extended state of the drone.
 */
void extended_state_cb(const mavros_msgs::ExtendedState::ConstPtr& msg)
{
	current_extended_state = *msg;
}

/**
 * Update the laser scan readings
 */
void laserscan_cb(const sensor_msgs::LaserScan::ConstPtr& msg)
{
	current_laserscan = *msg;
}

/**
 * Find if the ros node with "node_name" exists.
 * The node_name must not include the leading slash.
 *
 * 1. Get all of the node names from the master and store them in the all_nodes string vector.
 * 2. Then iterate through the node names and if the provided node name hasn't been found
 *    return 0. If found, return 1.
 */
int rosNodeExists(std::string node_name)
{
	ros::V_string all_nodes;
	ros::master::getNodes(all_nodes);
	for (std::vector<std::string>::const_iterator it = all_nodes.begin(); it != all_nodes.end(); ++it) {
		if (*it == '/' + node_name) {
			/* Node found */
			return 1;
		}
	}
	/* Node was not found */
	return 0;
}

/**
 * The main function
 */
int main(int argc, char** argv)
{
	std::string mavros_node;
	std::string rplidar_laser_topic;

	/**
	 * You must call one of the versions of ros::init() before using any other
	 * part of the ROS system. The third argument is the node name of your program.
	 */
	ros::init(argc, argv, "vangelis_drone");

	/**
	 * NodeHandle is the main access point to communications with the ROS system.
	 * The first NodeHandle constructed will fully initialize this node, and the last
	 * NodeHandle destructed will close down the node.
	 */
	ros::NodeHandle nh; // Create a global NodeHandle to communicate with other nodes
	ros::NodeHandle nh_p("~"); // Create a private NodeHandle to read the private parameters

	/**
	 * If we run several drones (e.g. in the simulator), we may be having multiple
	 * mavros nodes (one for each drone). Thus, make the current program a bit more
	 * flexible by accepting a command line parameter with the mavros node name that
	 * you want to control.
	 */
	if (nh_p.hasParam("mavros_node")) {
		/**
		 * If the mavros_node parameter is provided by the user, try to connect
		 * to the provided node.
		 *
		 * You can connect to mavros_2 node with the following command:
		 *     rosrun drone drone_node_cpp _mavros_node:=mavros_2
		 */
		nh_p.getParam("mavros_node", mavros_node);
		nh_p.deleteParam("mavros_node");

		if (mavros_node[0] == '/') {
			usage(argv[0]);
			ROS_ERROR("The mavros node name should not be starting with a slash (/).");
			ros::shutdown();
		}
	} else {
		/* Default: If no argument provided, connect to the mavros node. */
		mavros_node = "mavros";
	}

	/**
	 * Moreover, some nodes may be having an rplidar sensor, while some others may not.
	 * Thus, if the user provides a topic name with the rplidar_laser_topic parameter,
	 * try to use a 360 rplidar sensor. Otherwise, do not use an rplidar sensor.
	 */
	if (nh_p.hasParam("rplidar_laser_topic")) {
		nh_p.getParam("rplidar_laser_topic", rplidar_laser_topic);
		nh_p.deleteParam("rplidar_laser_topic");

		if (rplidar_laser_topic[0] == '/') {
			usage(argv[0]);
			ROS_ERROR("The rplidar_laser_topic name should not be starting with a slash (/).");
			ros::shutdown();
		}
	}

	/**
	 * Override the default ros sigint handler.
	 * This must be set after the first NodeHandle is created.
	 */
	signal(SIGINT, mySigIntHandler);

	/**
	 * Use ROS_INFO to print command line messages.
	 */
	ROS_INFO("Welcome to the test Drone ROS program!");
	ROS_INFO("Connecting to mavros node '%s'...", mavros_node.c_str());

	/**
	 * Ensure that the requested mavros node name exists.
	 */
	if (!rosNodeExists(mavros_node)) {
		ROS_ERROR("The requested '%1$s' node hasn't been found :(\n"
		          "Please check 'rosnode list' and ensure that the '%1$s' node exists.",
		          mavros_node.c_str());
		ros::shutdown();
	}

	/**
	 * TODO: Note that even if we have ensured at this point that the provided node
	 *       exists, we still haven't made sure that it is of type "mavros".
	 *       Even if I run this program with 'rosrun drone drone_node_cpp _mavros_node:=rosout'
	 *       it will still execute (as rosout node is always present), but it will get stuck
	 *       since the correct mavros topics cannot be found for the subscriptions below.
	 *
	 *       Thus, you need some more sanity checks to ensure that the chosen mavros_node
	 *       is indeed of the "mavros" type.
	 */
	ROS_INFO("Connected to mavros node '%s'", mavros_node.c_str());

	/**
	 * The subscribe() call is how you tell ROS that you want to receive messages
	 * on a given topic.  This invokes a call to the ROS master node, which keeps
	 * a registry of who is publishing and who is subscribing.  Messages are passed
	 * to a callback function, here called state_cb for the first subscriber, and
	 * extended_state_cb for the second subscriber. subscribe() returns a Subscriber
	 * object that you must hold on to until you want to unsubscribe. When all copies
	 * of the Subscriber object go out of scope, this callback will automatically be
	 * unsubscribed from this topic. This is why we still allocate the ros::Subscriber
	 * state_sub and extended_state_sub variables, even though these variables are
	 * typically unused (and your IDE/Compile may be complaining).
	 *
	 * The second parameter to the subscribe() function is the size of the message
	 * queue. If messages are arriving faster than they are being processed, this
	 * is the number of messages that will be buffered up before beginning to throw
	 * away the oldest ones.
	 */
	ros::Subscriber state_sub = nh.subscribe<mavros_msgs::State>(
	            getRosnodePath(mavros_node, "/state"), 10, state_cb);
	ros::Subscriber extended_state_sub = nh.subscribe<mavros_msgs::ExtendedState>(
	            getRosnodePath(mavros_node, "/extended_state"), 10, extended_state_cb);

	/**
	 * Subscribe to the rplidar topic if one has been provided by the user.
	 * The default laser topic name of the iris_rplidar model is the 'laser/scan'
	 * topic. Still, this has to be provided by the user since we do not want to
	 * subscribe to a laser topic at all if none has been provided by the user.
	 */
	ros::Subscriber laserscan_sub;
	if (!rplidar_laser_topic.empty()) {
		laserscan_sub = nh.subscribe<sensor_msgs::LaserScan>(
		            rplidar_laser_topic, 100, laserscan_cb);
	}

	/**
	 * The advertise() function is how you tell ROS that you want to publish on a
	 * given topic name. This invokes a call to the ROS master node, which keeps
	 * a registry of who is publishing and who is subscribing. After this advertise()
	 * call is made, the master node will notify anyone who is trying to subscribe
	 * to this topic name, and they will in turn negotiate a peer-to-peer connection
	 * with this node. advertise() returns a Publisher object which allows you to
	 * publish messages on that topic through a call to publish(). Once all copies
	 * of the returned Publisher object are destroyed, the topic will be
	 * automatically unadvertised.
	 *
	 * The second parameter to advertise() is the size of the message queue used for
	 * publishing messages. If messages are published more quickly than we can send
	 * them, the number here specifies how many messages to buffer up before throwing
	 * some away.
	 */
	ros::Publisher local_pos_pub = nh.advertise<geometry_msgs::PoseStamped>(
	            getRosnodePath(mavros_node, "/setpoint_position/local"), 10);


	/**
	 * The serviceClient() function is how you tell ROS to send a command to a
	 * service. More info about services and how they differ from topics can be
	 * found in this link: http://wiki.ros.org/Services
	 */

	ros::ServiceClient arming_client = nh.serviceClient<mavros_msgs::CommandBool>(
	            getRosnodePath(mavros_node, "/cmd/arming"));
	ros::ServiceClient landing_client = nh.serviceClient<mavros_msgs::CommandTOL>(
	            getRosnodePath(mavros_node, "/cmd/land"));
	ros::ServiceClient set_mode_client = nh.serviceClient<mavros_msgs::SetMode>(
	            getRosnodePath(mavros_node, "/set_mode"));

	/**
	 * The px4 flight stack has a timeout of 500ms between two Offboard commands.
	 * If this timeout is exceeded, the commander will fall back to the last mode
	 * the vehicle was in before entering Offboard mode. This is why the publishing
	 * rate MUST be faster than 2 Hz to also account for possible latencies. This
	 * is also the same reason why it is recommended to enter Offboard mode from
	 * Position mode, this way if the vehicle drops out of Offboard mode it will
	 * stop in its tracks and hover.
	 */
	ros::Rate rate(20.0);

	/* wait for FCU connection */
	while(rosok() && current_state.connected){
		ros::spinOnce();
		rate.sleep();
	}

	/**
	 * We are publishing setpoints to the /mavros/setpoint_position/local subscriber.
	 * Thus, we do not need to use GPS coordinates, but absolute values that are
	 * relative to the starting position ;)
	 *
	 * In this case, just takeoff and hover 2 meters from the ground (2m in the z axis)
	 */
	geometry_msgs::PoseStamped pose;
	pose.pose.position.x = 0;
	pose.pose.position.y = 0;
	pose.pose.position.z = 2;

	/**
	 * Before entering Offboard mode, you must have already started streaming setpoints.
	 * Otherwise the mode switch will be rejected.
	 * So send a few setpoints before starting. Here, 100 was chosen as an arbitrary amount.
	 */
	int num_setpoints = 100;
	ROS_INFO("Publishing a few setpoints before putting the vehicle in OFFBOARD mode.");
	ROS_INFO("This will take a few seconds.");
	for(int i = num_setpoints; rosok() && i > 0; --i){
		local_pos_pub.publish(pose);
		ros::spinOnce();
		rate.sleep();
	}
	ROS_INFO("%d setpoints published!", num_setpoints);

	/**
	 * Choose the custom "OFFBOARD" mode. A list of custom modes can be found here:
	 *   http://wiki.ros.org/mavros/CustomModes#PX4_native_flight_stack
	 */
	mavros_msgs::SetMode offb_set_mode;
	offb_set_mode.request.custom_mode = "OFFBOARD";

	/**
	 * In the following while loop we enter into flying mode. We attempt to switch
	 * to Offboard mode, after which we arm the quad to allow it to fly. We space out
	 * the service calls by 5 seconds so to not flood the autopilot with the requests.
	 * In the same loop, we continue sending the requested pose at the appropriate rate
	 * while changing the x position of the drone slowly with a fixed 0.01 meters on
	 * each iteration. We also print the data we read from the laser sensor (or we can
	 * program the drone to do something else based on the laser data). This while loop
	 * will be running forever (similar to the way microcontrollers operate) or until we
	 * press Ctrl+C to raise a SIGINT signal.
	 */
	mavros_msgs::CommandBool arm_cmd;
	arm_cmd.request.value = true;

	ros::Time last_request = ros::Time::now();

	while(rosok()){
		if (current_state.mode != "OFFBOARD" &&
		        (ros::Time::now() - last_request > ros::Duration(5.0))) {
			if( set_mode_client.call(offb_set_mode) &&
			        offb_set_mode.response.mode_sent) {
				ROS_INFO("OFFBOARD enabled");
			}
			last_request = ros::Time::now();
		} else {
			if (!current_state.armed) {
				if (ros::Time::now() - last_request > ros::Duration(5.0)) {
					if( arming_client.call(arm_cmd) &&
					        arm_cmd.response.success) {
						ROS_INFO("Vehicle armed");
					}
					last_request = ros::Time::now();
				}
			} else {
				/**
				 * Just print the data of the laser scan (just the ranges) if the user has provided
				 * an rplidar laser topic parameter.
				 */
				if (!rplidar_laser_topic.empty()) {
					for (std::vector<float>::const_iterator it = current_laserscan.ranges.begin();
					     it != current_laserscan.ranges.end();
					     ++it) {
						printf("%f ", *it);
					}
					printf("\n\n");
				}

				/**
				 * If the drone is already in "OFFBOARD" mode, on each iteration move the drone
				 * slightly with regards to its relative X position.
				 */
				pose.pose.position.x -= 0.01;
			}
		}

		local_pos_pub.publish(pose);

		/**
		 * ros::spinOnce() will enforce all the callbacks to be called from within
		 * this thread (the main one).
		 */
		ros::spinOnce();
		rate.sleep();
	}

	/**
	 * Performing pre-shutdown tasks.
	 *
	 * Basically landing the drone if still flying.
	 *
	 * TODO: It would probably be better if I set the mode to AUTO_RTL (Return to Land)
	 *       instead of calling the land command directly (at least in open spaces).
	 *
	 *       The description of AUTO_RTL for multirotors is: The multirotor returns in
	 *       a straight line on the current altitude (if higher than the home position
	 *       + loiter altitude) or on the loiter altitude (if higher than the current
	 *       altitude), then lands automatically.
	 */
	mavros_msgs::CommandTOL land_cmd;

	if (current_extended_state.landed_state == MAV_LANDED_STATE_IN_AIR) {
		if (landing_client.call(land_cmd) && land_cmd.response.success) {
			ROS_INFO("Vehicle received the landing command. Waiting to land...");
			/**
			 * Note that in the following loop I haven't used my rosok() function because a
			 * shutdown has already been initiated. Thus, in this case, ros::ok() will
			 * return 1 (because ros::shutdown() hasn't been called yet), but my rosok()
			 * function will return 0 (because the request_shutdown_flag has already been set
			 * to 1).
			 */
			while (ros::ok() && current_extended_state.landed_state != MAV_LANDED_STATE_ON_GROUND) {
				ros::spinOnce();
				rate.sleep();
			}
			/**
			 * ----- WARNING -----
			 * This is only some demo code.
			 * Note that "landed succesfully" here only means that the land detector
			 * (https://dev.px4.io/en/tutorials/land_detector.html) detects that the
			 * vehicle is landed. Now if your vehicle has crashed and not really
			 * "landed", that is your problem. You should implement additional sanity
			 * checks before any landing attempt.
			 */
			ROS_INFO("Vehicle landed succesfully.");
		} else {
			ROS_INFO("Failed to land vehicle.");
		}
	} else {
		ROS_INFO("Vehicle is already on land.");
	}

	/* Now we can shutdown and exit. */
	ROS_INFO("Bye bye!");
	ros::shutdown();
	return 0;
}
